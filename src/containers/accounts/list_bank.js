import React, { useState, useEffect, Fragment } from 'react'
import AutoCompleteSearch from '../../components/exrequest/client_search';
import { FolderPlus, Plus } from 'react-feather';
import LModel from '../../services/api';
import Filter from '../../components/ui/filter';
import GridLoading from '../../components/loading/exrequest_loading';
import NewBank from '../../components/account/new_bank';
import BankCard from '../../components/account/bank_card';
import Pagination from '../../components/ui/pagination';

import CircularLoading from '../../components/loading/circular_loading';
import Popup from '../../components/ui/popup';



const BankList = () => {

    const [loading, setLoading] = useState(false)
    const [bank, setBank] = useState([])
    const [bankInfo, setBankInfo] = useState({ previous: null, next: null, count: 0 })


    const [filter, setFilter] = useState(null)

    const [reload, setReload] = useState(false)
    const [toogleNew, setToogleNew] = useState(false)

    const [searchKey, setSearchKey] = useState("")
    const [circularloading, setCircularloading] = useState(false)
    const [paginationIndex, setPaginationIndex] = useState(1)

    const [editBank, setEditBank] = useState()
    const [showpop, setShowPop] = useState(false)
    const [deleteLoading, setDeleteLoading] = useState(false)




    const loadBank = () => {
        setLoading(true)
        LModel.find('banks', null, filter !== null ? `${filter}&name=${searchKey}&page=${1}` : `&name=${searchKey}&page=${paginationIndex}`)
            .then(res => {
                setLoading(false)
                if (res && res.results) {
                    setBank(res.results)
                    setBankInfo(res)
                }
            }).catch(e => {
                setLoading(false)
                console.log(e)
            })
    }


    useEffect(() => {
        loadBank()
    }, [searchKey, reload, filter, paginationIndex])


    useEffect(() => {
        if (editBank && editBank.id) {
            setToogleNew(true)
        }
        if (showpop) {
            setToogleNew(false)
        }
    }, [editBank])



    const deleteLoan = () => {
        if (editBank && editBank.id) {
            setDeleteLoading(true)
            LModel.destroy('banks', editBank.id)
                .then(res => {
                    // if (res && res.results)  {
                    setDeleteLoading(false)
                    setShowPop(false)
                    loadBank()
                    // }
                }).catch(e => {
                    setDeleteLoading(false)
                    console.log(e)
                })
        }
    }


    const LoanDeletePopUp = () => {

        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want delete accounts and its transactions under
                    <span className="text-blue-800 font-semibold italic mx-1">{editBank && editBank.name ? editBank.name : '--'}</span>
                    bank</p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => deleteLoan()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )
    }



    return (

        <Fragment>
            <div className="flex justify-between">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadBank}
                />

                <div className="inline-flex">

                    {!toogleNew ?
                        <div className="flex justify-end mx-3">
                            <button onClick={() => {
                                setEditBank()
                                setToogleNew(true)
                            }
                            } className="border rounded-md float-riht border-gray-600 p-2 my-1 hover:bg-green-600 hover:stroke-white stroke-green px-3  uppercase">
                                <Plus size={23} strokeWidth="4" className="mr-1 inline" />
                                    NEW Bank
                                </button>
                        </div> :
                        null
                    }

                    <Filter filter={filter} setFilter={setFilter} />

                </div>
            </div>


            {
                loading ?
                    <div className="flex justify-center">
                        <GridLoading />
                    </div>
                    :




                    <Fragment>


                        {!toogleNew ?

                            null
                            :
                            <NewBank closeSave={() => setToogleNew(false)} reloadPage={() => setReload(!reload)} editBank={editBank} />
                        }



                        {bank && !loading && bank.length > 0 ?
                            <div className="grid grid-cols-4 gap-4 mt-6 m-10">
                                {
                                    bank.map((bnk, idx) => {
                                        return <BankCard key={idx} res={bnk} setToEdit={(m) => setEditBank(m)} closeNew={() => setToogleNew(false)} setShowPop={() => setShowPop(true)}
                                        />
                                    })
                                }
                            </div>
                            :
                            <div className="w-full text-center p-4">

                                <div className="">
                                    <p className={searchKey === undefined ? 'hidden' : 'block text-gray-800 text-2xl font-thin'}> {`Not Found ${searchKey}`}</p>
                                    <p>
                                        <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-white" size={140} color={'gray'} strokeWidth={0.5} />
                                    </p>
                                </div>

                            </div>
                        }

                    </Fragment>
            }

            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<LoanDeletePopUp />}
                parentCss={'w-5/12 bg-white rounded p-6'}
            />

            <Pagination
                count={bankInfo.count}
                previous={bankInfo.previous}
                next={bankInfo.next}
                list={20}
                activeIndex={paginationIndex}
                setIndex={(i) => setPaginationIndex(i)}
            />


        </Fragment>

    )

}

export default BankList;