import React, { useEffect, useState } from 'react';
import LModel from '../../services/api'
import CircularLoading from '../../components/loading/circular_loading'
import { Search } from 'react-feather'

export default ({ label, labelStyle, divStyle, placeHolder, url, is_paginate, field, onValueSet }) => {

    const [searchKey, setSearchKey] = useState("")
    const [searchResult, setSearchResult] = useState([])
    const [circularLoading, setCircularLoading] = useState(false)



    useEffect(() => {

        if (searchKey.id) {
            onValueSet(searchKey)
        } else {
            onValueSet()
        }

        loadSearchResult()

    }, [searchKey])





    const loadSearchResult = () => {

        if (searchKey != null && !searchKey.id && searchKey.trim().length >= 1) {

            setCircularLoading(true)
            LModel.find(`${url}?${field}=${searchKey}`)
                .then(res => {
                    setCircularLoading(false)

                    if (is_paginate && res.results) {
                        setSearchResult(res.results)
                    } else {
                        setSearchResult(res)
                    }

                }).catch(err => {
                    // console.log(err)
                    setSearchResult([])
                    setCircularLoading(false)
                })
        }

    }



    const handlingSearchInputChange = (e) => {
        // alert(searchKey.length)

        if (e.target.value != null && e.target.value.trim().length > 1) {
            setSearchKey(e.target.value)

        } else if (e.target.value === "") {
            setSearchKey("")

            setSearchResult()
        } else {
            setSearchKey(e.target.value.trim())
        }


    }

    return (
        <div className={`${divStyle ? divStyle : 'w-full md:w-1/3 px-10 mb-6 md:mb-0'}`}>
            {
                label ?

                    <label className={`${labelStyle ? labelStyle : 'block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3'}`}>
                        {label}
                    </label>
                    :
                    null
            }
            <div className="relative py-1 w-full">
                <input className="appearance-none block w-full bg-gray-200 text-gray-800 border border-blue-800 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="search" placeholder={`${placeHolder ? placeHolder : ''}`}
                    onChange={handlingSearchInputChange}
                    value={searchKey && searchKey.id ? searchKey[field] : searchKey}
                />


                {
                    circularLoading ?
                        <CircularLoading loading={circularLoading} color={"bg-gray-600"} />
                        :
                        null
                }


                {
                    searchKey && searchKey.length > 0 && searchResult && searchResult.length > 0 ?
                        <div className="absolute w-full mt-1 border rounded bg-gray-100 z-10">
                            {
                                searchResult.map(sr => {
                                    return (
                                        <li key={sr.id} onClick={() => setSearchKey(sr)} className="block px-4 py-3 text-sm leading-5 text-gray-800 hover:bg-gray-200 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">{sr[field]}</li>
                                    )
                                })
                            }

                        </div>
                        :

                        <div>
                            <p className={searchKey && searchKey.length > 0 && searchResult && searchResult.length === 0 ? 'block' : 'hidden'}>  {`Not Found ${searchKey}`}</p>
                        </div>
                }

            </div>
        </div>

    )

}