import React from 'react'
import './c_loading.css'

export default ({ loading, color, txt, }) => {
    return (<div className={`inline-block ${!loading ? 'hidden' : ''}`}>
        {
            txt ?
            <p className={`text-2xl inline mx-4 ${color ? "text-" + (color.replace("bg-",'')) : "text-white"}`}>{txt ? txt : " "}</p>
            :
            null
        }
        
        <div className="sk-chase inline-block mx-2">
            <div className="sk-chase-dot"><p className={`${color ? color : "bg-white"}`}></p></div>
            <div className="sk-chase-dot"><p className={`${color ? color : "bg-white"}`}></p></div>
            <div className="sk-chase-dot"><p className={`${color ? color : "bg-white"}`}></p></div>
            <div className="sk-chase-dot"><p className={`${color ? color : "bg-white"}`}></p></div>
            <div className="sk-chase-dot"><p className={`${color ? color : "bg-white"}`}></p></div>
            <div className="sk-chase-dot"><p className={`${color ? color : "bg-white"}`}></p></div>
        </div>
    </div>
    )
}
