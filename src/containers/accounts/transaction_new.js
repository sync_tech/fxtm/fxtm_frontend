import React, { useEffect, useState } from 'react';
import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading';
import CustomField from '../../components/form/custom_field';



const NewTransaction = ({ toogleNTran, IsDeposite, nwTransaction, account, reloadPage }) => {


    const [saveLoading, setSaveLoading] = useState(false)
    const [newTransaction, setNewTransaction] = useState(nwTransaction && nwTransaction.id ? nwTransaction : null)


    function handleInputChange(ky, val) {
        let newT = { ...newTransaction }
        newT[ky] = val
        setNewTransaction(newT)
    }


    const saveTransaction = () => {

        if (newTransaction && newTransaction.id && newTransaction["account"] && (newTransaction["deposit"] || newTransaction["withdraw"])) {

            let newTran = { ...newTransaction }
            newTran["account"] = account['id']

            setSaveLoading(true)
            LModel.update('bank_transaction', newTransaction.id, newTran)
                .then(res => {
                    setSaveLoading(false)
                    toogleNTran(false)
                    reloadPage()
                }).catch(e => {
                    setSaveLoading(false)
                    console.log(e)
                })


        } else if (account && account.id && newTransaction && (newTransaction["deposit"] || newTransaction["withdraw"])) {

            let newTran = { ...newTransaction }
            newTran["account"] = account['id']

            setSaveLoading(true)
            LModel.create('bank_transaction/', newTran)
                .then(res => {
                    setSaveLoading(false)
                    toogleNTran(false)
                    // closeSave()
                    reloadPage()

                }
                ).catch(e => {
                    setSaveLoading(false)
                    toogleNTran(false)
                    console.log(e)
                })

        }
    }




    return (
        <div className={`bg-gray-300 rounded-md hover:shadow-md p-4 m-3 break-all relative w-10/12 'border-4 border-white hover:border-blue-400 ${IsDeposite ? 'float-right' : 'float-left'}`}>

            <CustomField
                name={`amount`}
                placeholder={`amount`}
                type="number"
                rules={{ required: true, min: 1 }}
                onBlur={() => console.log()}
                defaultValue={newTransaction && (newTransaction["deposit"] || newTransaction["withdraw"]) ? newTransaction["deposit"] || newTransaction["withdraw"] : ''}
                onChange={e => IsDeposite ? handleInputChange('deposit', e) : handleInputChange('withdraw', e)}
                inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-2 leading-tight focus:outline-none focus:bg-blue-100 focus:border-gray-500"
            />

            <CustomField
                name={`reason`}
                placeholder={`remind`}
                type="text"
                // rules={{ required: true }}
                onBlur={() => console.log()}
                defaultValue={newTransaction && newTransaction["reason"] ? newTransaction["reason"] : ''}
                onChange={e => handleInputChange('reason', e)}
                inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-2 leading-tight focus:outline-none focus:bg-blue-100 focus:border-gray-500"
            />
            {/* <CustomField
                name={`created_at`}
                placeholder={`Account Number`}
                type="date"
                onBlur={() => console.log()}
                defaultValue={newTransaction && newTransaction["bid"] ? newTransaction["bid"] : ''}
                onChange={e => handleInputChange('bid', e)}
                inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-2 leading-tight focus:outline-none focus:bg-blue-100 focus:border-gray-500"
            /> */}

            <div className="mt-2 flex justify-around">
                <button onClick={() => toogleNTran(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded px-2 mr-4 w-32">Cancel</button>
                <button onClick={() => saveTransaction()} className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded px-3 mr-4 w-32">{saveLoading ? <CircularLoading loading={saveLoading} /> : saveLoading ? "Update" : "Save"}</button>
            </div>

        </div>
    )
}

export default NewTransaction;