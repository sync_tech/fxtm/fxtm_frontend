import React from 'react';
import { Route } from 'react-router-dom';
import PrivateRoute from '../../components/route/private_route';
import { clientRoutes } from '../../routes';


export default () => {
    return (
        <div className="mx-4">
            {/* <div className="bg-white p-4 rounded min-h-screen border border-blue-200 border-2 shadow-lg"> */}
            <div className="p-4">

                {
                    clientRoutes.map((route, index) =>
                        route.private ? <PrivateRoute key={index} {...route} /> : <Route key={index} {...route} />
                    )
                }

            </div>
        </div>

    )
}