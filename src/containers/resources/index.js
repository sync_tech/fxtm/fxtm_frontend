import React, { useState, useEffect, Fragment } from 'react';
import TransactionList from './transaction_list';
import LModel from '../../services/api';
import CurrenciesCard from './currency_card'
import Filter from '../../components/ui/filter';
import AutoCompleteSearch from '../../components/exrequest/client_search';

const Resource = () => {


    const [loading, setLoading] = useState(false)
    const [parentACurrency, setParentACurrency] = useState()

    const [searchKey, setSearchKey] = useState("")
    const [filter, setFilter] = useState({ initialDate: '', finalDate: '' })
    const [paginationIndex, setPaginationIndex] = useState(1)
    // const [reload, setReload] = useState(false)
    const [circularloading, setCircularloading] = useState(false)


    const [ctransaction, setCTransaction] = useState([])
    const [ctransactionInfo, setCTransactionInfo] = useState({ previous: null, next: null, count: 0 })


    const loadCTransaction = () => {

        // 

        // `${parentACurrency && parentACurrency.code ? `currency__code=${parentACurrency.code}` : null}`)

        setLoading(true)
        LModel.find('creditByCurrency', null, filter && filter.initialDate ? `currency__code=${parentACurrency.code}&exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${1}` : `currency__code=${parentACurrency.code}&exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${paginationIndex}`)
            .then(res => {
                if (res && res.results) {
                    setLoading(false)
                    setCTransaction(res.results)
                    setCTransactionInfo(res)
                }
            }).catch(e => {
                setLoading(false)
                console.log(e)
            })
    }


    // const loadSearchResult = () => {
    //     setCircularloading(true)
    //     LModel.find(`creditByCurrency`, null, `currency__code=${parentACurrency.code}&exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}`)

    //         .then(res => {
    //             if (res && res.results) {
    //                 setCTransaction(res.results)
    //                 setCTransactionInfo(res)
    //                 setCircularloading(false)
    //             }
    //         }).catch(err => {
    //             console.log(err)
    //             setCircularloading(false)
    //         })

    // }


    useEffect(() => {
        // if (searchKey != null && searchKey.trim().length > 1) {
        //     loadSearchResult()
        // }

        if (parentACurrency && parentACurrency.code) {
            loadCTransaction()
        }


    }, [searchKey, filter, paginationIndex, parentACurrency])






    return (
        <div className="mx-4 py-4">
            <div className="flex justify-between px-4">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadCTransaction}
                />
                <div className="inline-flex">
                    <Filter filter={filter} setFilter={setFilter} />
                </div>

            </div>
            <div className="flex py-6">
                <CurrenciesCard
                    setParentActiveCurrency={(crcy) => setParentACurrency(crcy)}
                />


                <div className="p-4 ml-3 rounded-sm w-full bg-white">
                    <TransactionList
                        filter={filter}
                        loading={loading}
                        ctransaction={ctransaction}
                        ctransactionInfo={ctransactionInfo}
                        searchKey={searchKey}
                        paginationIndex={paginationIndex}
                        setPaginationIndex={(idx) => setPaginationIndex(idx)}

                    />
                </div>
            </div>
        </div>
    )
}


export default Resource;