import { LOGIN_REQUEST, LOGIN_REQUEST_SUCCESS, LOGIN_REQUEST_ERROR, LOGOUT_REQUEST, LOGOUT_REQUEST_SUCCESS, LOGOUT_REQUEST_ERROR, CHECK_IS_LOGGEDIN, CHECK_IS_LOGGEDIN_ERROR, CHECK_IS_LOGGEDIN_SUCCESS, CHECK_IS_ROLE_SET, CHECK_IS_ROLE_SET_SUCCESS, CHECK_IS_ROLE_SET_ERROR } from "../constants/action_constants";
import AuthSerivce from "../services/auth_service";
export const initialState = {
  isLoggedIn: AuthSerivce.is_logged_in(),
  user: AuthSerivce.get_user(),
  roles: AuthSerivce.get_groups(),
  loading: false,
  error: null,
  loginRequested: false,
  loginSuccess: false,
  logoutSuccess: false,
  logoutError: false,
  roleSetSuccess: false,
  registerSuccess: false
}

export default (state, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        loginRequested: true,
      }
    case LOGIN_REQUEST_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        user: action.payload.data.user,
        loading: false,
        loginSuccess: true
      }
    case LOGIN_REQUEST_ERROR:
      return {
        ...state,
        error: action.payload.error,
        loading: false
      }
    case LOGOUT_REQUEST:
      return {
        ...state,
        loading: true
      }
    case LOGOUT_REQUEST_SUCCESS:
      return {
        ...initialState,
        logoutSuccess: true
      }
    case LOGOUT_REQUEST_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        logoutError: true
      }
    case CHECK_IS_LOGGEDIN:
      return {
        ...state,
        loading: true
      }
    case CHECK_IS_LOGGEDIN_ERROR:
      return {
        ...state,
        loading: false,
        isLoggedIn: false
      }
    case CHECK_IS_LOGGEDIN_SUCCESS:
      return {
        ...state,
        loading: false,
        isLoggedIn: true
      }
    case CHECK_IS_ROLE_SET:
      return {
        ...state,
        loading: true,
      }
    case CHECK_IS_ROLE_SET_ERROR:
      return {
        ...state,
        loading: false,
        isLoggedIn: false,
        roleSetSuccess: false,
      }
    case CHECK_IS_ROLE_SET_SUCCESS:
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
        roleSetSuccess: true,
        roles: action.payload.roles
      }

    default:
      return state
  }
}