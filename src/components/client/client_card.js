import React, { useState, useEffect, useRef } from 'react';
import { Star, Plus, Edit, Trash2, MoreVertical } from 'react-feather'
import LModel from '../../services/api'
import CircularLoading from '../../components/loading/circular_loading'
import { useHistory } from '../../providers/history_provider'


const ClientCard = ({ data, reload, setShowPop, closeNew, setActiveClient }) => {

    const [loading, setLoading] = useState(false)
    const { history } = useHistory()
    const [show, setShow] = useState(false)


    const moreMenu = useRef(null);
    DropDownToggler(moreMenu);

    function DropDownToggler(ref) {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setShow(false)
            } else {
                setShow(true)
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside, true);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside, true);
            };
        });
    }

    const setToVip = () => {
        LModel.update('clients', data.id, { vip: !(data.vip) })
            .then(res => {
                if (res.id) {
                    setLoading(false)
                    reload()
                }
            }
            ).catch(e => {
                setLoading(false)
                console.log(e)
                reload()
            })
    }


    return (
        data && data.id ?

            <div className="rounded bg-white border border-2 hover:shadow-xl shadow-md m-3 p-3 relative">
                <div className="">



                    <div className="absolute right-0 top-0 inline-flex">
                        <div className="hover:stroke-white hover:stroke-white text-gray-500 hover:text-gray-700 p-1">
                            {loading ?
                                <CircularLoading loading={loading} color={'bg-gray-600'} />
                                :
                                <Star onClick={setToVip} fill={data.vip ? "#f6e05e" : 'white'} className="inline" strokeWidth={1} />
                            }
                        </div>
                        {show ?
                            <div ref={moreMenu} className="p-1">
                                <div onClick={(e) => {
                                    e.stopPropagation();
                                    closeNew()
                                    setActiveClient(data)
                                }}
                                    className="text-gray-700 rounded hover:bg-blue-100 hover:text-blue-500">
                                    <Edit size={20} className="inline m-1" />
                                </div>
                                <div onClick={(e) => {
                                    e.stopPropagation();
                                    setActiveClient(data)
                                    closeNew()
                                    setShowPop()
                                }}
                                    className="text-gray-700 rounded hover:bg-blue-100 hover:text-red-600">
                                    <Trash2 size={20} className="inline m-1" />
                                </div>

                            </div>
                            :
                            <div ref={moreMenu} onClick={() => setShow(!show)} className=" text-gray-600 rounded hover:text-gray-800 m-1 p-1 hover:bg-gray-200">
                                <MoreVertical size={20} className="my-auto" />
                            </div>

                        }
                    </div>




                    <div className="flex">
                        <div className="w-12 h-12 rounded-full mr-4 shadow bg-gray-600 relative">
                            <div className="w-12 h-12 flex justify-center items-center">
                                <p className="text-2xl text-white">{data.name[0]}</p>
                            </div>
                        </div>
                        <div className="h-12">
                            <p className="h-full flex items-center text-gray-700 text-lg font-semibold"> {data.name}</p>
                        </div>
                    </div>

                </div>

                {/* <div className="flex justify-evenly m-3">
                     <p className="text-gray-700 font-thin"> $USD</p>
                     <p className="text-gray-700 font-thin"> $GBP</p>
                     <p className="text-gray-700 font-thin"> $CNY</p>
                     <p className="text-gray-700 font-thin"> $AED</p>
                 </div> */}

                {/* <div className="flex justify-around m-3 ">
                 <p className="text-gray-700 font-semibold text-lg bg-teal-300 rounded-md p-1">#15</p>
                 <p className="text-gray-700 font-semibold text-lg p-1">+10,500</p>
                 <p className="text-green-700 font-semibold text-lg p-1">0.00</p>

             </div> */}
                <div className="text-right m-2">

                    {/* <div className="inline-flex"> */}
                    {/* <li className="inline-flex px-4 rounded border border-2 border-blue-600 text-xs p-1 text-blue-800 hver:bg-blue-600 hver:text-white">
                         <p className="rounded border border-2">Request</p>
                         <ChevronDown size={20} strokeWidth="2" className="hover:bg-blue-400 hover:text-white rounded ml-2" color="gray" />
                     </li> */}
                    {/* <li className="inline-flex rounded border border-2 border-blue-600 text-xs p-1 text-blue-800 hover:bg-blue-600 hover:text-white"> */}
                    {/* <ChevronDown size={14} strokeWidth="2" className="hover:bg-blue-600 hover:text-white" color="gray"/> */}
                    {/* </li> */}

                </div>
                {/* <li className="inline-block px-4 rounded border border-2 border-blue-600 text-xs p-1 text-blue-800 hover:bg-blue-700 hover:text-white">
                 New
             </li> */}

                {/* <li className="inline-block px-4 rounded border text-xs p-1 text-white bg-blue-700 hover:bg-blue-500 mx-4">
                     Transactions
             </li> */}
                {/* </div> */}
                {/* border rounded-md float-riht border-gray-600 p-2 my-1 hover:bg-green-700 hover:stroke-white stroke-green px-3 */}
                <div className="flex justify-evenly my-1">
                    <div className="inline-flex">
                        <button onClick={() => history.push('./exrequest', { client: data })} className="px-3 rounded-l bg-gray-300  border text-xs p-1 text-gray-700 hover:bg-green-700 hover:text-white">
                            Ex Request
                 </button>
                        <button onClick={() => history.push('./exrequest', { client: data, new: true })} className="px-2 rounded-r bg-gray-300 border text-xs p-1 text-gray-700 hover:bg-green-700 hover:text-white">
                            <Plus size={15} strokeWidth="4" />
                        </button>
                    </div>
                    <button onClick={() => history.push('./report', { client: data, new: true })} className="px-3 rounded border text-xs p-1 text-gray-700 hover:bg-blue-700 hover:text-white">
                        Transactions
                 </button>

                </div>

            </div>


            :
            null
    )
}

export default ClientCard;





