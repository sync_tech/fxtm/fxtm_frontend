import React from 'react'
import axios from 'axios';
// import { toast } from 'react-toastify'
import AuthService from './auth_service';
import { API_BASE_URL as URL} from '../constants/api_constants'
// import { useHistory } from '../providers/history_provider';

  const AxiosInstance = axios.create({
    baseURL: URL,
    proxy: false,
    timeout: 15000,
    headers: {
      'Content-Type': 'application/json',
    },
  });



    AxiosInstance.interceptors.request.use(
      async (config) => {
          config.baseURL = URL
          try {
            // config.url = `${config.url}${config.url.split('?').length === 2 ? '&' : '?'}access_token=${AuthService.is_logged_in() ? AuthService.get_token().token : ''}`            
            config.headers.common['Authorization'] = `${AuthService.get_token() && AuthService.get_token().token ? 'Token ' + AuthService.get_token().token : null }`
          } catch (error) {
            console.log('error getting token', error)
          }

          // if (AuthService.get_token().token) {
          // } else {
          //   config.headers.common['Authorization'] = null;
          // }
          
        
        //   console.log(config.url, 'urll....................')
        // config.url = `${config.url}?access_token${await AsyncStorage.getItem('token')}`
          return config
      },
      request => {
        // console.log('Requsting ..... ')
        // console.log(request)
        return request;
      },
      error => Promise.reject(error)
    );

  AxiosInstance.interceptors.response.use(
    response => {
      
      return response.data
    },
    (error) => {
      if (!error.response) {
        console.error('Network Error!');
      } else if (error.response.status === 500) {
        console.error('Server Error!');
      } else if (error.response.status === 404) {
        // toast.info('Not Found!')
      } else if (error.response.status === 401) {
        console.error('Not Authorized!');
        console.error('red Authorized!');
        // history.push("/auth")
        // appHistory.replace('/Unauthorized')
        return window.location.href = '/auth'
      } else if (error.response.status === 400){
        console.error('Error, Try Again!');
      } else if (error.response.status === 422){
        console.error('Some inputs are invalid, try again!');
      } else if (error.response.status === 403){
        console.error('Error, You dont have enough parmission !')
        alert('Permission Error')
      } else {
       
      }
      
      // handle the errors due to the status code here
      return Promise.reject(error.response);
    },
  );




export default AxiosInstance;