import React,{useState} from 'react'
import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading'

export default ({ activeClient, setShowPop, reload}) => {

    const [deleteLoading, setDeleteLoading] = useState()

    const deleteClient = () => {
        if (activeClient && activeClient.id) {
            setDeleteLoading(true)
            LModel.destroy('client', activeClient.id)
                .then(res => {
                    // if (res && res.results)  {
                    setDeleteLoading(false)
                    setShowPop(false)
                    reload()
                    // }
                }).catch(e => {
                    setDeleteLoading(false)
                    console.log(e)
                })
        }
    }

    return (
        <div className="w-full">
            <div className="text-left border-b-2 px-4">
                <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
            </div>
            <div className="text-center p-3">
                <p className="p-2 text-gray-700 text-xl">Are you sure, do you want delete
            <span className="text-blue-800 font-semibold italic mx-1">{activeClient && activeClient.name ? activeClient.name : '--'}</span>
                </p>
            </div>
            <div className="w-full text-right">
                <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                <button onClick={() => deleteClient()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}</button>
            </div>
        </div>
    )
}