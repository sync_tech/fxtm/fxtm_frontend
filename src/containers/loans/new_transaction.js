import React, { useState, useEffect } from 'react';
import CustomField from '../../components/form/custom_field';
import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading';

export default ({ closeSave, reloadPage, loan, is_paid, activeT }) => {

    // const [newLoanTransaction, setnewLoanTransaction] = useState(loan && loan.id ? loan : { name: "", repayment: "" })
    const [newLoanTransaction, setnewLoanTransaction] = useState(activeT && activeT.id ? activeT : { name: "", amount: "" })
    const [loading, setLoading] = useState(false)

    const saveRequest = () => {

        if (newLoanTransaction && newLoanTransaction.amount,newLoanTransaction.id) {

            setLoading(true)
            LModel.update('loan_transaction', newLoanTransaction.id, newLoanTransaction)
                .then(res => {
                    setLoading(false)
                    closeSave()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    closeSave()
                })

        } else if (newLoanTransaction && newLoanTransaction.amount && loan && loan.id) {

            let newT = { ...newLoanTransaction }
            newT['loan'] = loan["id"]
            newT['loan_type'] = is_paid

            setLoading(true)
            LModel.create('loan_transaction/', newT)
                .then(res => {
                    setLoading(false)
                    closeSave()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    closeSave()
                })
        }
    }


    const handleInputChange = (name, val) => {
        let tempLoan = { ...newLoanTransaction }
        tempLoan[name] = val
        setnewLoanTransaction(tempLoan)
    }


    return (
        <div className="rounded border border-2 border-blue-900 flex flex-col p-2 my-2 py-6 items-center">

            <CustomField
                name={`name`}
                placeholder={`${newLoanTransaction['name'] ? newLoanTransaction['name'] : 'name'}`}
                type="text"
                onBlur={() => console.log()}
                defaultValue={newLoanTransaction["name"]}
                onChange={e => handleInputChange('name', e)}
                divStyle="w-6/12 px-1 mb-6 md:mb-0"
                labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            />

            <CustomField
                name={`amount`}
                placeholder={`${newLoanTransaction['amount'] ? newLoanTransaction['taken'] : 'amount'}`}
                type="number"
                onBlur={() => console.log()}
                defaultValue={newLoanTransaction["amount"]}
                onChange={e => handleInputChange('amount', e)}
                divStyle="w-6/12 px-1 mb-6 md:mb-0"
                labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            />


            <div className="w-full text-center my-2 px-8">
                <button onClick={() => saveRequest()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{loading ? <CircularLoading loading={loading} /> : newLoanTransaction.id ? "Update" : "Save"}</button>
                <button onClick={() => closeSave()} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
            </div>

        </div>
    )

}