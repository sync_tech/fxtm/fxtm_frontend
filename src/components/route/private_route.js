import React from "react";
import { Route } from "react-router-dom";
import { useAuth } from "../../providers/auth_provider";
import { useHistory } from "../../providers/history_provider";
import { useCheckedSetTitle } from "../../providers/app_provider";

export default (props) => {
    const { authState } = useAuth()
    const { isLoggedIn } = authState
    const { history } = useHistory()
    // console.log('> private route', isLoggedIn)


    // console.log(authState)

    // console.log(authState)

    if(!isLoggedIn)
        history.push('/auth/login')
    return (
        <Route {...props} />
    )
}