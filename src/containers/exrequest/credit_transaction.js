import React, { useState, useEffect, Fragment } from 'react';
import moment from 'moment';
import { FolderPlus, Plus, ChevronDown, Trash2, Edit3 } from 'react-feather';
import LModel from '../../services/api';
import Loading from '../../components/loading/dot_loading';

import NewCredit from './new_credit'
import { useHistory } from '../../providers/history_provider';
import { MoneyFormat } from '../../services/money_format';
import Popup from '../../components/ui/popup';
import CircularLoading from '../../components/loading/circular_loading';


const CreditTransaction = ({ setTotal, id, setIsCompleted, reloadAllPage, setExReq }) => {


    const [toogleNew, setToogleNew] = useState(false)
    const [creditLoading, setCreditLoading] = useState(false)
    const [credit, setCredit] = useState({ creditTransactions: [] })
    const [totalCredit, setTotalCredit] = useState(0)
    const [reload, setReload] = useState(false)
    const { history } = useHistory()

    const [showpopdel, setShowPopDel] = useState(false)
    const [deleteLoading, setDeleteLoading] = useState(false)
    const [activeTransaction, setActiveTransaction] = useState()


    const deleteT = () => {
        if (activeTransaction && activeTransaction.id) {
            setDeleteLoading(true)
            LModel.destroy(`${activeTransaction.birr ? 'credits' : 'debits'}`, activeTransaction.id)
                .then(res => {
                    setDeleteLoading(false)
                    // if (res && res.id) {
                    setShowPopDel(false)
                    // setReloadPage(!reloadPage)
                    // }
                }).catch(e => {
                    setDeleteLoading(false)
                    console.log(e)
                })
        }

    }


    const DeleteTransaction = ({ msg }) => {
        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want to delete {msg}

                    </p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPopDel(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => deleteT()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )
    }




    const loadCredit = (id) => {

        const reducer = (acc, cVal) => acc + cVal;

        setCreditLoading(true)
        LModel.find('credit', id)
            .then(res => {
                setCreditLoading(false)
                setCredit(res)

                let credit_tot = res && res.creditTransactions && res.creditTransactions.length > 0 ?
                    res.creditTransactions.map(c => parseFloat(c.birr)).reduce(reducer) : null

                isNaN(credit_tot) ? setTotalCredit(0) : setTotalCredit(credit_tot)

                res && res.is_completed ? setIsCompleted(res.is_completed) : setIsCompleted(false)
                let exReq = { ...res }
                exReq.creditTransactions = exReq && exReq.creditTransactions ? [] : null
                setExReq(exReq)
            }
            ).catch(e => {
                setCreditLoading(false)
                console.log(e)
                history.push('./')
            })

    }



    useEffect(() => {
        if (id) {
            loadCredit(id)
            setTotal(totalCredit)
        }
    }, [reload, reloadAllPage])


    useEffect(() => {
        setTotal(totalCredit)
        setIsCompleted(credit.is_completed)
    }, [totalCredit])

    useEffect(() => {
        if (activeTransaction && activeTransaction.id) {
            setToogleNew(true)
        }
        if (showpopdel) {
            setToogleNew(false)
        }
    }, [activeTransaction])

    return (
        <div className="">

            {
                creditLoading ?
                    <Loading loading={creditLoading} color={'bg-gray-600'} />
                    :
                    credit.creditTransactions.length > 0 ?
                        <Fragment>
                            {!toogleNew ?
                                <button onClick={() => {
                                    setActiveTransaction()
                                    setToogleNew(true)
                                }} className={`border rounded-md ${credit && credit.is_supplier ? 'float-right' : 'float-left'} border-gray-600 p-2 my-3 hover:bg-green-700 hover:stroke-white stroke-green px-3`}>
                                    <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                                            ADD NEW
                                    </button> : null}
                            <table className="w-full bg-orange-100">
                                <thead className="bg-blue-700">
                                    <tr className="border p-2 text-white">
                                        <th className="border-r border-gray-400 p-2 text-center">#</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Date</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Amount</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Rate <ChevronDown className="inline float-right hover:bg-blue-800 m-1" size={18} /></th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Birr</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Edit</th>
                                    </tr>
                                </thead>
                                <tbody className="">
                                    {
                                        credit.creditTransactions.map((cred, index) => (
                                            <tr key={index} className={`border cursor-pointer hover:bg-gray-200 text-center`}>
                                                <td className="border-r p-2 text-sm">{index + 1}</td>
                                                <td className="border-r text-gray-900 text-sm">{moment(cred.created_at).format('ll')}</td>
                                                <td className="border-r text-gray-900 text-sm">{MoneyFormat(cred.requested_amount)}</td>
                                                <td className="border-r text-gray-900 text-sm">{MoneyFormat(cred.rate) + '  USD'}</td>
                                                <td className="border-r text-gray-900 text-sm">{MoneyFormat(cred.birr)}</td>
                                                <td className="border-r text-gray-900 text-sm">
                                                    <div className="inline-flex">
                                                        <p onClick={() => {
                                                            setActiveTransaction(cred)
                                                            setToogleNew(true)
                                                        }} className="text-gray-700 rounded p-1  hover:text-white hover:bg-blue-400"> <Edit3 size={18} className={"inline"} /> </p>
                                                        <p onClick={() => {
                                                            setActiveTransaction(cred)
                                                            setShowPopDel(true)
                                                        }} className="text-gray-700 rounded p-1 hover:text-white hover:bg-red-400"> <Trash2 size={18} className={"inline"} /> </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        ))
                                    }

                                    <tr className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                        <td className="border-r p-2 text-sm">{credit.creditTransactions.length + 1}</td>
                                        <td className="border-r text-gray-900 text-sm">{}</td>
                                        <td className="border-r text-gray-900 text-sm">{}</td>
                                        <td className="border-r text-gray-900 text-sm">{}</td>
                                        <td className="border-r text-gray-900 text-sm">{}</td>
                                        <td className="border-r text-gray-900 text-sm">{}</td>
                                    </tr>
                                    <tr className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                        <td colSpan="2" className="border-r text-gray-900 p-2 text-lg">{'Total'}</td>
                                        <td colSpan="4" className="border-r text-green-800 font-semibold text-lg">{MoneyFormat(totalCredit)}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </Fragment>
                        :
                        <div className="w-full text-center p-4">

                            <div className="">
                                <p>
                                    <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-teal-100" size={140} color={'gray'} strokeWidth={0.5} />
                                </p>

                                <em>{credit && credit.is_supplier ? 'Debit' : 'Credit'} transaction list is empty.</em>

                            </div>
                        </div>
            }


            {
                toogleNew ?
                    <NewCredit toogleNq={() => setToogleNew(!toogleNew)} exReq={credit.id} reloadPage={() => setReload(!reload)} activeC={activeTransaction} /> : null
            }

            <Popup
                show={showpopdel}
                close={() => setShowPopDel(!showpopdel)}
                children={<DeleteTransaction
                    msg={
                        activeTransaction && activeTransaction.id && activeTransaction.birr ?
                            <Fragment>
                                Credit Transaction of birr <span className="text-blue-800 font-semibold italic mx-1"> {activeTransaction && activeTransaction.id ? MoneyFormat(activeTransaction.birr) : ''}</span></Fragment>
                            :
                            <Fragment>
                                Debit Transaction of birr <span className="text-blue-800 font-semibold italic mx-1"> {activeTransaction && activeTransaction.id ? MoneyFormat(activeTransaction.amount) : ''}</span></Fragment>
                    }
                />}

                parentCss={'w-5/12 bg-white rounded p-6'}
            />
        </div>
    )

}

export default CreditTransaction