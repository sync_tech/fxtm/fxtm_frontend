import React, { useEffect, useState } from 'react';
import LModel from '../../services/api';
import AutoCompleteDropdown from '../../components/ui/auto_complete_dropdown';
import CircularLoading from '../../components/loading/circular_loading';

export default ({ closeSave, reloadPage, auto_set_client, activeExReq }) => {

    const [client, setClient] = useState(auto_set_client && auto_set_client.id ? auto_set_client : activeExReq && activeExReq.id ? {...activeExReq["client"],...{"is_supplier" : activeExReq["is_supplier"]} } : {})
    const [saveLoading, setSaveLoading] = useState(false)
    // const [show, setShow] = useState(false)

    const saveExRequest = () => {

        if (client && activeExReq && activeExReq.id && client.id) {
            setSaveLoading(true)
            LModel.update('request', activeExReq.id, { client: client.id, is_supplier: client["is_supplier"] && client["is_supplier"] === true ? true : false })
                .then(res => {
                    setSaveLoading(false)
                    closeSave()
                    reloadPage()

                }).catch(e => {
                    setSaveLoading(false)
                    console.log(e)
                    closeSave()
                })
        } else if (client && client.id) {
            setSaveLoading(true)
            LModel.create('request/', { client: client.id, is_supplier: client["is_supplier"] && client["is_supplier"] === true ? true : false })
                .then(res => {
                    setSaveLoading(false)
                    closeSave()
                    reloadPage()

                }).catch(e => {
                    setSaveLoading(false)
                    console.log(e)
                    closeSave()
                })
        }

    }


    function handleSwith(v) {
        let cl = { ...client }
        cl["is_supplier"] = v
        setClient(cl)
    }



    useEffect(() => {
        if (auto_set_client && auto_set_client.id) {
            setClient(auto_set_client)
        }
        if (activeExReq && activeExReq.id && activeExReq.client) {
            let c = {...activeExReq.client, ...{"is_supplier" : activeExReq["is_supplier"]} }
            setClient(c)
        }
    }, [auto_set_client, activeExReq])


    return (

        <div className="flex justify-end">
            <div className="rounded border border-blue-400 w-1/2 p-2">
                <AutoCompleteDropdown
                    label={'Client Name'}
                    divStyle={'w-full py-1 px-10 mb-6 md:mb-0'}
                    field={'name'}
                    url={'clients'}
                    is_paginate={true}
                    placeHolder={client && client.id ? client.name : null}
                    onValueSet={(vl) => vl && vl.id ? setClient(vl) : setClient()}
                />


                <div className="flex justify-center items-center h-16">
                    <label className={`text-gray-600 font-semibold ${client && client.is_supplier ? 'text-gray-700' : ''} `}> Supplier </label>
                    {
                        client && client.is_supplier ?
                            <span onClick={() => handleSwith(false)} className="border rounded-full border-grey flex items-center cursor-pointer w-24 bg-gray-500 justify-start mx-4">
                                <span className="rounded-full border w-12 h-12 border-grey shadow-inner bg-white shadow">
                                </span>
                            </span>
                            :
                            <span onClick={() => handleSwith(true)} className="border rounded-full border-grey flex items-center cursor-pointer w-24 bg-blue-500 justify-end mx-4">
                                <span className="rounded-full border w-12 h-12 border-grey shadow-inner bg-white shadow">
                                </span>
                            </span>
                    }
                    <label className={`text-gray-600 font-semibold ${client && !client.is_supplier ? 'text-blue-700' : ''}`}> Normal </label>



                </div>



                <div className={`w-full text-center my-4 px-4`}>
                    <button onClick={() => saveExRequest()} type="submit" className={`disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32 ${client && client.id ? null : 'opacity-50 cursor-not-allowed'}`}>{saveLoading ? <CircularLoading loading={saveLoading} /> : activeExReq && activeExReq.id ? "Update" : "Save"}</button>
                    <button onClick={() => closeSave()} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                </div>
            </div>
        </div>
    )

}





