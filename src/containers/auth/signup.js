import React, { useState, useEffect } from 'react'
// import AuthLayout from '../../components/Auth/AuthLayout';
import { Link } from 'react-router-dom'
import { useAuth } from '../../providers/auth_provider';
import { useHistory } from '../../providers/history_provider';
import CustomForm from '../../components/form/custom_form';




const Signup = (props) => {

    const defaultForm = {
        firstname: '',
        lastname: '',
        email: '',
        password: '',
        confirmpassword: ''
    }

    const [form, setForm] = useState(defaultForm)

    const handleFormChange = (e) => {
        let newForm = { ...form }
        newForm[e.target.name] = e.target.value
        setForm(newForm)
    }

    const { signup, authState } = useAuth()

    const { history } = useHistory()
    const handleSubmit = (data, errors) => {
        signup(data)
    }

    useEffect(() => {
        if (authState.loginSuccess) {
            props.history.push('/home')
        }
    }, [authState.loginSuccess, props.history])

    // redirect after login success
    useEffect(() => {
        if (authState.loginSuccess) history.push('/home')
    }, [authState.loginSuccess, history])


    const fields = [
        {
            name: "firstname",
            placeholder: "First Name",
            type: 'text',
            value: "",
            rules: {
                required: true
            },
            label: "First Name",
            labelStyle: "text-gray-600 text-sm",
            divStyle: "my-2 w-1/2"
        },
        {
            name: "lastname",
            placeholder: "Last Name",
            type: 'text',
            value: "",
            rules: {
                required: true
            },
            label: "Last Name",
            labelStyle: "text-gray-600 text-sm",
            divStyle: "my-2 w-1/2"
        },
        {
            name: "email",
            placeholder: "email",
            type: 'email',
            value: "",
            rules: {
                required: true
            },
            label: "Email",
            labelStyle: "text-gray-600 text-sm",
            divStyle: "my-2"
        },
        {
            name: "password",
            placeholder: "Password",
            type: 'password',
            rules: {
                required: true
            },
            label: "Password",
            labelStyle: "text-gray-600 text-sm",
            divStyle: "my-2"
        },
        {
            name: "confirmpassword",
            placeholder: "Confirm Password",
            type: 'password',
            rules: {
                required: true
            },
            label: "Confirm Password",
            labelStyle: "text-gray-600 text-sm",
            divStyle: "my-2"
        }
    ]


    return (
        <section className="w-full">
            

      <div className="pb-6">
        <h1 className="text-2xl font-semibold text-blue-700  text-center">Join our community !</h1>
        <p className="text-center text-sm text-gray-600">Already have an account? <Link to="/auth/login" className="hover:text-teal-800"> Login </Link></p>
      </div>
            <CustomForm fields={fields} onSubmit={handleSubmit}>

                {/* <input type="checkbox" defaultChecked={remember} onChange={value => console.log(value)}/><span className="text-blue-800 pl-2">Remember me</span> */}
                <button type="submit" className="block w-full bg-blue-700 mt-4 mb-2 p-2 text-white font-bold rounded">Join our Community</button>
            </CustomForm>
        </section>
    )
}


export default Signup;