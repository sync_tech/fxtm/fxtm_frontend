import React, { useState, useEffect, Fragment, useRef } from 'react';
import moment from 'moment';
import { MoneyFormat } from '../../services/money_format';
import { Trash2, Edit, MoreVertical, } from 'react-feather';
import { useHistory } from '../../providers/history_provider';

export default ({ res, setToEdit, closeNew, setShowPop }) => {

    const [show, setShow] = useState(false)
    const { history } = useHistory()

    const moreMenu = useRef(null);
    DropDownToggler(moreMenu);

    function DropDownToggler(ref) {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setShow(false)
            } else {
                setShow(true)
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside, true);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside, true);
            };
        });
    }


    return (
        <div onClick={() => history.push(`/dashboard/account/${res.id}`, { bank: res })} className={`rounded bg-white border-2 border-gray-400 p-1 hover:shadow-lg flex flex-col justify-between relative`}>
            <div className="">

                <div ref={moreMenu} className="absolute right-0 top-0">
                    {show ?
                        <div className="p-1">

                            <div onClick={(e) => {
                                e.stopPropagation();
                                closeNew()
                                setToEdit(res)
                            }}
                                className="text-gray-700 rounded hover:bg-blue-100 hover:text-blue-500">
                                <Edit size={20} className="inline m-1" />
                            </div>
                            <div onClick={(e) => {
                                e.stopPropagation();
                                setToEdit(res)
                                setShowPop()
                            }}
                                className="text-gray-700 rounded hover:bg-blue-100 hover:text-red-600">
                                <Trash2 size={20} className="inline m-1" />
                            </div>

                        </div>
                        :
                        <div className=" text-gray-500 rounded hover:text-gray-800 m-1 p-1 hover:bg-gray-200">
                            <MoreVertical onClick={() => setShow(!show)} size={20} className="my-auto" />
                        </div>

                    }
                </div>


                <h2 className="text-gray-700 text-xl text-center underline p-1 capitalize">{res.name}</h2>

            </div>
            <p className="m-1 font-thin text-center text-sm text-gray-700">{res && res.branch ? res.branch : '--'}</p>

            <p className="m-2 font-thin text-center text-gray-700">{MoneyFormat(res && res.balance ? 200000 : 200000)}</p>

        </div >

    )

}