import React, { useState, useEffect } from 'react'
// import AuthLayout from '../../components/Auth/AuthLayout';
import { Link } from 'react-router-dom'
import { useAuth } from '../../providers/auth_provider';
import { useHistory } from '../../providers/history_provider';
import CustomForm from '../../components/form/custom_form';
import Loading from '../../components/loading/dot_loading';
import LModel from '../../services/api';
// import { toast } from 'react-toastify';

const ForgetPassword =  (props) => {

  const [cError, setCError] = useState('')
  const [loading, setLoading] = useState(false)
  const { history } = useHistory()

  const fields = [
    {
      name: "email",
      placeholder: "email",
      type: 'email',
      value: "",
      label: "Email",
      rules: {
        required: true
      },
      labelStyle: "text-gray-600 text-sm",
      divStyle: "my-2"

    }
  ]

  const handleSubmit = (data, errors) => {
    if (data.username !== "" && data.username !== undefined) {
      history.push('/auth/reset_password');
    }

    // if (!data.newPassword){
    //   return
    // }
    // if (data.newPassword !== data.cNewPassword){
    //   setCError('Passwords do not match')
    //   return
    // }
    // setLoading(true)
    // LModel.create('users/reset-password', data)
    //   .then(response => {
    //   //   toast.success('Password reset successful')
    //     history.push('/auth/logout')
    //     setLoading(false)
    //   })
    //   .catch(error => {
    //     setCError('Error Try again!')
    //     setLoading(false)
    //   })
  }

  return (
    <section className="w-full">
      {/* <h3 className="text-blue-700 text-xl">Reset Password</h3> */}
      <div className="pb-2">
        <h1 className="text-2xl font-normal text-blue-700  text-center">Reset Password</h1>
      </div>
      <CustomForm fields={fields} onSubmit={handleSubmit}>

        {
          cError ?
            <span className="text-red-700">{cError}</span>
            : null
        }
        <button type="submit" className="block w-full bg-blue-700 mt-4 mb-2 p-2 text-white font-bold rounded">{loading ? <Loading loading={loading} /> : 'Reset'}</button>
      </CustomForm>
      <div className="text-center">
        <p className="text-gray-600 text-sm">Don't get an email? <Link to="#" className="hover:text-teal-800"> Resend </Link></p>
      </div>
    </section>
  )
}

export default ForgetPassword;