import React, { useState, useEffect } from 'react';
import CustomField from '../../components/form/custom_field';
import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading';

const NewUser = ({ toogleNq, reloadPage, editUser }) => {

    // const [newMemo, setNewMemo] = useState(editMemo && editMemo.id ? editMemo : { title: "", body: "" })

    const [newUser, setNewUser] = useState(editUser && editUser.id ? editUser : { first_name: "", last_name: "", username: "", password: "", groups:[]})
    const [loading, setLoading] = useState(false)




    function handleInputChange(ky, val) {
        let newD = { ...newUser }
        newD[ky] = val
        setNewUser(newD)
    }


    const saveRequest = () => {
        if (newUser && newUser.password && newUser.username) {
            setLoading(true)
            LModel.create('manage-user/', newUser)
                .then(res => {
                    setLoading(false)
                    // setRequest(res)
                    toogleNq()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    toogleNq()
                })

        }

        // console.log(newUser)

    }

    // first_name
    // last_name
    // username
    // password
    // groups

    // enseno

    return (
        <div className="rounded border border-2 border-blue-900 flex flex-col p-2 my-2">

            <div className="flex m-2 justify-evenly">
                <CustomField
                    name="first_name"
                    placeholder=""
                    defaultValue={newUser["first_name"]}
                    type="text"
                    onBlur={() => console.log()}
                    label="First Name"
                    onChange={e => handleInputChange('first_name', e)}
                    divStyle="w-4/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />
                <CustomField
                    name="last_name"
                    placeholder=""
                    defaultValue={newUser['last_name']}
                    type="text"
                    onBlur={() => console.log()}
                    label="Last Name"
                    onChange={e => handleInputChange('last_name', e)}
                    divStyle="w-4/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />
            </div>
            <div className="flex m-2 justify-evenly">
                <CustomField
                    name="username"
                    placeholder=""
                    defaultValue={newUser['username']}
                    type="text"
                    onBlur={() => console.log()}
                    label="User Name"
                    rules={{ required: true }}
                    onChange={e => handleInputChange('username', e)}
                    divStyle="w-4/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none bg-white focus:bg-white focus:border-gray-500"
                />
                <CustomField
                    name="password"
                    placeholder=""
                    defaultValue={newUser['password']}
                    type="password"
                    onBlur={() => console.log()}
                    label="Password"
                    // rules={{ required: true, min: 1 }}
                    onChange={e => handleInputChange('password', e)}
                    divStyle="w-4/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none bg-white focus:bg-white focus:border-gray-500"
                />
            </div>


            <div className="w-full text-right my-4 px-8">
                <button onClick={() => saveRequest()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{loading ? <CircularLoading loading={loading} /> : "Save"}</button>
                <button onClick={() => toogleNq()} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
            </div>

        </div>
    
    )
}

export default NewUser;
