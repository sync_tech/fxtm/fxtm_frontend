import React from 'react'
import ContentLoader from 'react-content-loader'

const News = props => (
  <ContentLoader
    viewBox="0 0 200 550"
    width={200}
    height={550}
    speed={1}
    backgroundColor="#FFF"

    title="Loading..."
    {...props}
  >
    <rect x="20" y="10" rx="5" ry="5" width="170" height="110" />

    <rect x="20" y="140" rx="5" ry="5" width="170" height="110" />
    
    <rect x="20" y="270" rx="5" ry="5" width="170" height="110" />
    
    <rect x="20" y="400" rx="5" ry="5" width="170" height="110" />
    
    <rect x="20" y="530" rx="5" ry="5" width="170" height="110" />
    
    <rect x="20" y="650" rx="5" ry="5" width="170" height="110" />

  </ContentLoader>
)

News.metadata = {
  name: 'Arthur Falcão',
  github: 'arthurfalcao',
  description: 'News List',
  filename: 'News',
}

export default News