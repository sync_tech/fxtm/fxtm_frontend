import React, { useState, useEffect, Fragment, useRef } from 'react';
import moment from 'moment';
import { MoneyFormat } from '../../services/money_format';
import { Trash2, Edit, MoreVertical, Edit2 } from 'react-feather';
import { useHistory } from '../../providers/history_provider';

export default ({ res, setToEdit, closeNew, setShowPop }) => {

    const [show, setShow] = useState(false)
    const { history } = useHistory()

    const moreMenu = useRef(null);
    DropDownToggler(moreMenu);

    function DropDownToggler(ref) {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setShow(false)
            } else {
                setShow(true)
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside, true);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside, true);
            };
        });
    }



    return (
        <div onClick={() => history.push(`/dashboard/loan/${res.id}`)} className={`rounded bg-white border-2 border-gray-400 p-1 hover:shadow-lg flex flex-col justify-between relative`}>
            <div className="">

                <div ref={moreMenu} className="absolute right-0 top-0">
                    {show ?
                        <div className="p-1">

                            <div onClick={(e) => {
                                e.stopPropagation();
                                closeNew()
                                setToEdit(res)
                            }}
                                className="text-gray-700 rounded hover:bg-blue-100 hover:text-blue-500">
                                <Edit size={20} className="inline m-1" />
                            </div>
                            <div onClick={(e) => {
                                e.stopPropagation();
                                setToEdit(res)
                                setShowPop()
                            }}
                                className="text-gray-700 rounded hover:bg-blue-100 hover:text-red-600">
                                <Trash2 size={20} className="inline m-1" />
                            </div>

                        </div>
                        :
                        <div className=" text-gray-500 rounded hover:text-gray-800 m-1 p-1 hover:bg-gray-200">
                            <MoreVertical onClick={() => setShow(!show)} size={18} className="my-auto" />
                        </div>

                    }
                </div>


                <h2 className="text-gray-800 text-lg text-center underline p-1 capitalize">{res.name}</h2>

            </div>
            <p className="m-1 font-thin text-center text-sm text-gray-700">{res.reason && res.reason.length > 30 ? res.reason.substring(0, 50) + ' ...' : res.reason}</p>


            <div className="flex justify-center p-2">
                <div className="w-1/2 text-center">
                    <p className="m-2 font-thin text-red-600">{MoneyFormat(res && res.taken_amount ? res.taken_amount : 0)}</p>

                </div>
                <div className="w-1 rounded bg-gray-500 my-2"></div>
                <div className="w-1/2 text-center">
                    <p className="m-2 font-thin text-green-800">{MoneyFormat(res && res.paid_amount ? res.paid_amount : 0)}</p>
                </div>
            </div>

        </div >

    )

}