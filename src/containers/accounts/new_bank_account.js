import React, { useState, useEffect, Fragment, useRef } from 'react';
import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading';
import CustomField from '../../components/form/custom_field';



const NewAccount = ({ toogleNAcc, activeBank, activeAcc, reloadPage }) => {


    const [saveLoading, setSaveLoading] = useState(false)
    const [newAccount, seNewAccount] = useState(activeAcc ? activeAcc : { holder: "", bid: "" })


    function handleInputChange(ky, val) {
        let newA = { ...newAccount }
        newA[ky] = val
        seNewAccount(newA)
    }

    const saveEditBankAccount = () => {

        if (newAccount && newAccount.holder && newAccount.bid && newAccount.id && activeBank) {

            setSaveLoading(true)
            LModel.update('accounts', newAccount.id, newAccount)
                .then(res => {
                    setSaveLoading(false)
                    toogleNAcc(false)
                    reloadPage()
                }).catch(e => {
                    setSaveLoading(false)
                    console.log(e)
                })

        } else if (activeBank && newAccount.holder && newAccount.bid ) {

            let newAcc = { ...newAccount }
            newAcc["bank"] = activeBank.id

            setSaveLoading(true)
            LModel.create('accounts/', newAcc)
                .then(res => {
                    setSaveLoading(false)
                    toogleNAcc(false)
                    // closeSave()
                    reloadPage()

                }
                ).catch(e => {
                    setSaveLoading(false)
                    toogleNAcc(false)
                    console.log(e)
                })

        }
    }



    return (
            <div className={`bg-white rounded-md hover:shadow-lg p-2 mb-3 break-all relative  'border-4 border-white hover:border-blue-400 `}>

                <CustomField
                    name={`holder`}
                    placeholder={`Account Holder`}
                    type="text"
                    rules={{ required: true}}
                    onBlur={() => console.log()}
                    defaultValue={newAccount && newAccount["holder"] ? newAccount["holder"] : ''}
                    onChange={e => handleInputChange('holder', e)}
                    inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-2 leading-tight focus:outline-none focus:bg-blue-100 focus:border-gray-500"
                />

                <CustomField
                    name={`bid`}
                    placeholder={`Account Number`}
                    type="number"
                    rules={{ required: true}}
                    onBlur={() => console.log()}
                    defaultValue={newAccount && newAccount["bid"] ? newAccount["bid"] : ''}
                    onChange={e => handleInputChange('bid', e)}
                    inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-2 leading-tight focus:outline-none focus:bg-blue-100 focus:border-gray-500"
                />

                <div className="my-2 flex justify-around">
                    <button onClick={() => toogleNAcc(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white text-xs font-bold rounded px-2">Cancel</button>
                    <button onClick={() => saveEditBankAccount()} className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded text-xs px-3">{saveLoading ? <CircularLoading loading={saveLoading} /> : saveLoading ? "Update" : "Save"}</button>
                </div>

            </div>
    )
}

export default NewAccount;