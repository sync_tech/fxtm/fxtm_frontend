import React, { createContext, useReducer, useMemo, useContext, useState, useEffect } from 'react'
import globalReducer, { initialState } from '../reducers/global_reducer'
import { uploadFile, setTitle } from '../actions/global_actions'

export const AppContext = createContext()

export const AppProvider = (props) => {
    const [state, dispatch] = useReducer(globalReducer, initialState)
    const value = useMemo(() => [state, dispatch], [state])
    return <AppContext.Provider value={value} {...props} />
}

export const useApp = () => {
    const context = useContext(AppContext)
    if (!context) {
        throw new Error(`useApp must be used within a AppProvider`)
    }
    const [state, dispatch] = context
    const [toasts, setToasts] = useState([])
    const [device, setDevice] = useState('laptop')
    const [sideNavExpanded, toggleSideNavExpanded] = useState(false)

    useEffect(()=> {
        let windowWidth = window.innerWidth
        let device_ = 'laptop'
        if (windowWidth > 1440) {
            device_ = 'laptopxl'
        } else if (windowWidth > 1024 && device_ !== 'laptopxl') {
            device_ = 'laptopl'
        } else if (windowWidth > 768 && device_ !== 'laptopl') {
            device_ = 'laptop'
        } else if (windowWidth > 425 && device_ !== 'laptop') {
            device_ = 'tablet'
        } else {
            device_ = 'mobile'
        }
        setDevice(device_)
    })

    useEffect(() => {
        let expanded = false
        switch (device) {
            case 'mobile':
            case 'tablet':
                expanded = false;
                break;
            case 'laptop':
            case 'laptopl':
            case 'laptopxl':
                expanded = true
            default:
                break;
        }
        toggleSideNavExpanded(expanded)
    },[device])
    
    const upload = (container, file, cb) => uploadFile(dispatch)(container, file, cb)
    const setPageTitle = (title) => setTitle(dispatch)(title)
    const showToast = (toast) => setToasts([...toasts, toast])
    
    return {
        state,
        // dispatch,
        upload,
        setPageTitle,
        showToast,
        toasts,
        sideNavExpanded,
        toggleSideNavExpanded,
    }
}

export const useCheckedSetTitle = (title) => {
    const { setPageTitle } = useApp()
    const {pageTitle, changeTitle} = useState()
    const [isSet, setIsSet] = useState(false)

    useEffect(() => {
        // console.log('setting title')
        if(!isSet){
        setPageTitle(title)
        setIsSet(true)
        }
        
    },[isSet, title, setPageTitle])
}