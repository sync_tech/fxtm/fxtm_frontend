import React, { useState, useEffect, Fragment } from 'react';
import TransactionList from './transaction_list';
import LModel from '../../services/api';
import AccountCard from './bank_selection'
import Filter from '../../components/ui/filter';
import { X } from 'react-feather';
import AutoCompleteSearch from '../../components/exrequest/client_search';
import { useHistory } from '../../providers/history_provider'


const BankTransaction = (props) => {

    const [loading, setLoading] = useState(false)
    const [parentACurrency, setParentACurrency] = useState()

    const [searchKey, setSearchKey] = useState("")
    const [filter, setFilter] = useState({ initialDate: '', finalDate: '' })
    const [paginationIndex, setPaginationIndex] = useState(1)
    // const [reload, setReload] = useState(false)
    const [circularloading, setCircularloading] = useState(false)


    const [ctransaction, setCTransaction] = useState([])
    const [ctransactionInfo, setCTransactionInfo] = useState({ previous: null, next: null, count: 0 })




    const [bankTransaction, setBankTransaction] = useState()
    const [bankTransacionLoad, setBankTransactionLoad] = useState(false)


    const [reloadParentPage, setReloadParentPage] = useState(false)

    const { history } = useHistory();


    const loadCTransaction = () => {

        // 

        // `${parentACurrency && parentACurrency.code ? `currency__code=${parentACurrency.code}` : null}`)

        setLoading(true)
        LModel.find('creditByCurrency', null, filter && filter.initialDate ? `currency__code=${parentACurrency.code}&exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${1}` : `currency__code=${parentACurrency.code}&exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${paginationIndex}`)
            .then(res => {
                if (res && res.results) {
                    setLoading(false)
                    setCTransaction(res.results)
                    setCTransactionInfo(res)
                }
            }).catch(e => {
                setLoading(false)
                console.log(e)
            })
    }


    const loadBankTransaction = () => {
        setBankTransactionLoad(true)
        LModel.find(`creditByCurrency`, null, `currency__code=${parentACurrency.code}&exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}`)

            .then(res => {
                setBankTransactionLoad(false)
                if (res && res.results) {
                    setBankTransaction(res.results)
                    // setCTransactionInfo(res)
                }
            }).catch(err => {
                console.log(err)
                setBankTransactionLoad(false)
            })

    }


    useEffect(() => {
        // if (searchKey != null && searchKey.trim().length > 1) {
        //     loadSearchResult()
        // }

        if (parentACurrency && parentACurrency.name) {
            loadCTransaction()
        }


    }, [searchKey, filter, paginationIndex, parentACurrency])






    return (
        <div className="-mx-4">
            {/* // mx-4 py-4 */}
            <div className="flex justify-between">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadCTransaction}
                />
                <div className="inline-flex">
                    <Filter filter={filter} setFilter={setFilter} />
                    <button onClick={() => history.push('/dashboard/account')} className="border mx-3 rounded border border-gray-300 p-2 bg-white hover:bg-gray-500 hover:stroke-white stroke-gray-500 my-auto">
                        <X size={22} strokeWidth="4" />
                    </button>
                </div>

            </div>


            <div className="flex py-6">
                <AccountCard
                    setParentActiveCurrency={(crcy) => setParentACurrency(crcy)}
                    parentProp={props}
                    reloadParentPage={reloadParentPage}
                />


                <div className="p-4 ml-3 rounded-sm w-full bg-white">
                    <TransactionList
                        activeAccount={parentACurrency}
                        setActiveAccount={(cr) => setParentACurrency(cr)}
                        reloadParentPage={() => setReloadParentPage(!reloadParentPage)}
                    />
                </div>
            </div>
        </div>
    )
}


export default BankTransaction;