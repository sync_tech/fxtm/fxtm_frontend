import React, { useState, useEffect } from 'react';
import AutoCompleteDropdown from '../../components/ui/auto_complete_dropdown';
import CustomField from '../../components/form/custom_field';
import LModel from '../../services/api';
import Loading from '../../components/loading/dot_loading';
import CircularLoading from '../../components/loading/circular_loading';


const NewCredit = ({ toogleNq, exReq, reloadPage, activeC }) => {

    
    const [exRequest, setExRequest] = useState(exReq)
    const [requestedAmount, setRequestedAmount] = useState(activeC && activeC.requested_amount ? activeC["requested_amount"] : 1)
    const [rate, setRate] = useState(activeC && activeC.rate ? activeC["rate"] : 1)
    const [birr, setBirr] = useState(1)
    const [currency, setCurrency] = useState({})
    const [loading, setLoading] = useState(false)
    const [bank, setBank] = useState(null)


    // function handleInputChange(ky, val) {
    //     let newC = { ...newCredit }
    //     newC[ky] = val
    //     setNewCredit(newC)
    // }



    useEffect(() => {


        if (rate && rate > 0) {

            let cca = rate * parseFloat(requestedAmount)
            setBirr(cca)

        } else {

            if (currency && currency.rate) {
                setBirr(parseFloat(requestedAmount) * parseFloat(currency.rate))
            } else if (requestedAmount) {
                setBirr(parseFloat(requestedAmount) * 1)
            } else {
                setBirr(1)
            }
        }

    }, [currency, requestedAmount, rate])




    const saveRequest = () => {


        let data = {
            rate: rate ? rate : currency && currency.rate ? currency.rate : 1,
            birr: birr,
            requested_amount: requestedAmount,
            currency: currency.id,
            exRequest: exRequest,
            bank: bank ? bank.id : null
        }


        if (data && currency && currency.id) {

            setLoading(true)
            LModel.create('credits/', data)
                .then(res => {
                    setLoading(false)
                    // setRequest(res)
                    toogleNq()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    toogleNq()
                })

        }
    }


    return (
        <div className="rounded border border-2 border-blue-900 flex flex-col p-2 my-2">

            <div className="flex m-2 justify-evenly">

                <CustomField
                    name="requested_amount"
                    placeholder=""
                    type="number"
                    onBlur={() => console.log()}
                    label="Amount"
                    defaultValue={requestedAmount}
                    onChange={e => setRequestedAmount(e)}
                    rules={{ required: true, min: 1 }}
                    divStyle="w-6/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />

                <AutoCompleteDropdown
                    label={'Currency Type'}
                    divStyle={'w-4/12 px-1 mb-6 md:mb-0'}
                    field={'code'}
                    url={'currency'}
                    onValueSet={(vl) => vl && vl.id ? setCurrency(vl) : setCurrency()}
                />
            </div>


            <div className="flex m-2 justify-evenly">


                <CustomField
                    name="rate"
                    placeholder={currency && currency.rate ? currency.rate : 1}
                    type="number"
                    onBlur={() => console.log()}
                    label="Rate"
                    // defaultValue={rate}
                    onChange={e => setRate(e)}
                    divStyle="w-4/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />

                <div className="w-6/12 px-1 mb-6 md:mb-0">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3 self-center" htmlFor="birr">Birr</label>
                    <div className="py-1 w-full">
                        <input className="w-full p-2 bg-transparent rounded border border-blue-800 appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="" name="birr" autoComplete="off" readOnly
                            value={birr} />
                    </div>
                </div>
            </div>

            <div className="w-full text-right my-4 px-8">
                <button onClick={() => saveRequest()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{loading ? <CircularLoading loading={loading} /> : "Save"}</button>
                <button onClick={() => toogleNq()} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
            </div>

        </div >
    )
}

export default NewCredit;
