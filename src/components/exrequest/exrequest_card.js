import React, { useState, useRef, useEffect, Fragment } from 'react'
import moment from 'moment';
import { MoneyFormat } from '../../services/money_format';
import { useHistory } from '../../providers/history_provider';
import { MoreVertical, Edit, Trash2, Bookmark } from 'react-feather'


const ExRequestCard = ({ req, setShowPop, closeNew, setActiveExReq }) => {

    const { history } = useHistory()
    const [show, setShow] = useState(false)

    const moreMenu = useRef(null);
    DropDownToggler(moreMenu);

    function DropDownToggler(ref) {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setShow(false)
            } else {
                setShow(true)
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside, true);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside, true);
            };
        });
    }



    return (

        <div className="relative">
            <div ref={moreMenu} className="absolute right-0 top-0">

                {show ?
                    <div className="p-1">
                        <div onClick={(e) => {
                            e.stopPropagation();
                            closeNew()
                            setActiveExReq(req)
                        }}
                            className="text-gray-700 rounded hover:bg-blue-100 hover:text-blue-500 ">
                            <Edit size={20} className="inline m-1" />
                        </div>
                        <div onClick={(e) => {
                            e.stopPropagation();
                            setActiveExReq(req)
                            closeNew()
                            setShowPop()
                        }}
                            className="text-gray-700 rounded hover:bg-blue-100 hover:text-red-600">
                            <Trash2 size={20} className="inline m-1" />
                        </div>

                    </div>
                    :
                    <div onClick={() => setShow(!show)} className=" text-gray-600 rounded hover:text-gray-800 m-1 p-1 hover:bg-gray-200">
                        <MoreVertical size={20} className="my-auto" />
                    </div>

                }
            </div>

            {req && req.is_supplier && req.is_supplier === true ?
                <div className="absolute left-0 top-0 text-yellow-500 m-1 p-1">
                    <Bookmark size={20} className="my-auto" />
                </div>
                :
                null
            }


            <div onClick={() => history.push(`/dashboard/exrequest/${req.id}`)}
                className={`rounded bg-white border-4 ${req.is_completed ? 'border-green-500' : 'border-gray-500'} p-1 hover:shadow-lg`}>





                <h2 className="text-gray-800 text-lg text-center underline p-1">{req.client && req.client.name}</h2>
                <div className="text-center p-2">
                    <p className="text-lg font-semibold text-gray-700 break-words">{moment(req.name).isValid() ? moment(req.name).format("MMMM Do YY") : req.name}</p>
                </div>

                {
                    req && req.is_supplier ?
                        <Fragment>
                            <div className="flex justify-center p-1">
                                <div className="w-1/2 text-center">
                                    <p className="m-1 font-thin text-gray-700">{MoneyFormat(req.transactions.debitTransaction)}</p>
                                </div>
                                <div className="w-1 rounded bg-gray-500"></div>
                                <div className="w-1/2 text-center">
                                    <p className="m-1 font-thin text-gray-700 break-words">{MoneyFormat(req.transactions.creditTransaction)}</p>
                                </div>
                            </div>
                            <p className={`m-1 font-semibold text-center text-xl ${req.transactions.debitTransaction > req.transactions.creditTransaction ? 'text-red-500' : 'text-gray-700'}`}>{MoneyFormat(req.transactions.creditTransaction - req.transactions.debitTransaction)}</p>
                        </Fragment>
                        :
                        <Fragment>
                            <div className="flex justify-center p-1">
                                <div className="w-1/2 text-center">
                                    <p className="m-1 font-thin text-gray-700 break-words">{MoneyFormat(req.transactions.creditTransaction)}</p>
                                </div>
                                <div className="w-1 rounded bg-gray-500"></div>
                                <div className="w-1/2 text-center">
                                    <p className="m-1 font-thin text-gray-700">{MoneyFormat(req.transactions.debitTransaction)}</p>
                                </div>
                            </div>
                            <p className={`m-1 font-semibold text-center text-xl ${req.transactions.debitTransaction > req.transactions.creditTransaction ? 'text-red-500' : 'text-gray-700'}`}>{MoneyFormat(req.transactions.creditTransaction - req.transactions.debitTransaction)}</p>
                        </Fragment>
                }


                {
                    req.is_completed ?
                        <p className="text-green-700 text-center text-xs">Complete on {moment().format('ll')}</p>
                        :
                        <p className="text-gray-700 text-center text-xs">Not completed</p>
                }
            </div>
        </div>
    )
}


export default ExRequestCard;