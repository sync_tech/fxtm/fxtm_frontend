import React, { useState, useEffect, Fragment } from 'react';
import { Folder, ChevronDown } from 'react-feather';
import AutoCompleteSearch from '../../components/exrequest/client_search';
import Filter from '../../components/ui/filter';
import LModel from '../../services/api';
import moment from 'moment';
import TableLoading from '../../components/loading/table_loading';
import Pagination from '../../components/ui/pagination'
import { MoneyFormat } from '../../services/money_format';


const CreditTransaction = () => {

    const [ctransaction, setCTransaction] = useState([])
    const [ctransactionInfo, setCTransactionInfo] = useState({ previous: null, next: null, count: 0 })

    const [loading, setLoading] = useState(false)

    const [reload, setReload] = useState(false)
    const [searchKey, setSearchKey] = useState("")
    const [circularloading, setCircularloading] = useState(false)

    const [filter, setFilter] = useState({ initialDate: '', finalDate: '' })
    const [paginationIndex, setPaginationIndex] = useState(1)


    const loadCTransaction = () => {

        setLoading(true)
        LModel.find('creditLedger', null, filter && filter.initialDate ? `created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${1}` : `created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${paginationIndex}`)
            .then(res => {
                if (res && res.results) {
                    setLoading(false)
                    setCTransaction(res.results)
                    setCTransactionInfo(res)
                }
            }).catch(e => {
                setLoading(false)
                console.log(e)
            })
    }


    const loadSearchResult = () => {
        setCircularloading(true)
        LModel.find(`creditLedger`, null, `exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}`)

            .then(res => {
                if (res && res.results) {
                    setCTransaction(res.results)
                    setCTransactionInfo(res)
                    setCircularloading(false)
                }
            }).catch(err => {
                console.log(err)
                setCircularloading(false)
            })

    }

    // useEffect(() => {
    //     loadCTransaction()
    // }, [])

    useEffect(() => {
        if (searchKey != null && searchKey.trim().length > 1) {
            loadSearchResult()
        }
        else {
            loadCTransaction()
        }
    }, [searchKey, reload, filter, paginationIndex])





    return (

        <Fragment>


            <div className="flex justify-between">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadSearchResult}
                />
                <div className="inline-flex">
                    <Filter filter={filter} setFilter={setFilter} />
                </div>
            </div>


            {
                loading ?
                    <div className="flex justify-center">
                        <TableLoading />
                    </div>
                    :
                    ctransaction && ctransaction.length > 0 ?
                        <table className="w-full mt-8 bg-white">
                            <thead className="bg-blue-700">
                                <tr className="border p-2 text-white">
                                    <th className="border-r border-gray-400 p-2 text-center">#</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Client</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Amount</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Rate</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">currency</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Birr</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Transactor<ChevronDown className="inline float-right hover:bg-blue-800 m-1" size={18} /></th>
                                    <th className="border-r border-gray-400 font-semibold text-center">On</th>
                                    {/* <th className="border-r border-gray-400 pr-2"><FontAwesomeIcon icon={faPlusCircle} onClick={() => history.push('/dashboard/vehicles/new')} title="Add New Vehicle" className="text-4xl float-right hover:text-blue-800 text-blue-500 hover:shadow-outline border rounded-full h-7 w-7" /></th> */}
                                </tr>
                            </thead>
                            <tbody className="">
                                {
                                    ctransaction.map((tran, index) => (
                                        <tr key={index} className={`border cursor-pointer hover:bg-gray-200 text-center`}>
                                            <td className="border-r p-2 text-sm">{filter && filter.initialDate ? index + 1 : index + 1 + ((paginationIndex - 1) * 20)}</td>

                                            <td className="border-r text-gray-900 text-sm">{tran && tran.exRequest && tran.exRequest.client && tran.exRequest.client.name ? tran.exRequest.client.name : '--'}</td>
                                            <td className="border-r text-gray-900 text-sm">{MoneyFormat(tran.requested_amount)}</td>
                                            <td className="border-r text-gray-900 text-sm">{tran.rate}</td>
                                            <td className="border-r text-gray-900 text-sm">{tran.currency && tran.currency.code ?
                                                <Fragment>
                                                    <img src={tran.currency.flag} className="w-5 h-4 inline mx-1" />  {tran.currency.code}
                                                </Fragment>
                                                :
                                                '--'}
                                            </td>
                                            <td className="border-r text-gray-900 font-semibold text-sm">{MoneyFormat(tran.birr)}</td>
                                            <td className="border-r text-gray-900 text-sm">{'Jon'}</td>
                                            <td className="border-r p-2 flex justify-evenly">
                                                {/* <Link to={`vehicles/detail/${v.id}`} className={`active:outline-none p-2 bg-transparent rounded hover:text-white hover:bg-blue-500 text-blue-700 px-4 mr-2`} title={`Click to view detail`}>More...</Link> &nbsp; */}
                                                {/* <Link to={`vehicles/edit/${v.id}`} className={`active:outline-none p-2 bg-transparent rounded hover:text-white focus:shadow-outline hover:bg-blue-500 text-blue-700 px-4`} title={`Click to edit`}>Edit</Link> */}
                                                <p className="my-auto text-sm text-gray-900">{moment(tran.created_at).format('ll')}</p>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                        :
                        <div className="w-full text-center p-10">

                            <div className="">
                                <p>
                                    <Folder className="inline-flex text-center rounded-lg m-2" size={140} color={'gray'} strokeWidth={0.5} />
                                </p>
                                <em>Transaction under <span className="italic">{searchKey}</span> is not found.</em>
                            </div>
                        </div>
            }


            <Pagination
                count={ctransactionInfo.count}
                previous={ctransactionInfo.previous}
                next={ctransactionInfo.next}
                list={20}
                activeIndex={paginationIndex}
                setIndex={(i) => setPaginationIndex(i)}
            />
        </Fragment>


    )
}

export default CreditTransaction;


// Questions: Transaction list should separate or merge togther ??