import React, { useState, useEffect, Fragment } from 'react';
import { X, Square, CheckSquare } from 'react-feather';
import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading';
import moment from 'moment';
import { useHistory } from '../../providers/history_provider'

import CreditTransaction from './credit_transaction';
import DebitTransaction from './debit_transaction';
import DotLoading from '../../components/loading/dot_loading'

import Popup from '../../components/ui/popup';
import { MoneyFormat } from '../../services/money_format';

const ExRequestTransaction = ({ match }) => {

    const [reloadPage, setReloadPage] = useState(false)
    const [totCredit, setTotCredit] = useState(0)
    const [totDebit, setTotDebit] = useState(0)
    const [isCompleted, setIsCompleted] = useState(false)
    const [isCompleteLoading, setIsCompleteLoding] = useState(false)
    const { history } = useHistory()
    const [exReq, setExReq] = useState()
    const [showpop, setShowPop] = useState(false)

    const [showpopdel, setShowPopDel] = useState(false)
    const [deleteLoading, setDeleteLoading] = useState(false)
    const [activeTransaction, setActiveTransaction] = useState()



    function validateCompeletion() {
        if (isCompleted) {
            markAsComplete()
        } else {
            // if()
            if (Math.abs(totCredit - totDebit) >= 50) {
                setShowPop(true)
            } else {
                markAsComplete()
            }
        }
    }


    const markAsComplete = () => {
        if (match && match.params && match.params.id) {
            setIsCompleteLoding(true)
            LModel.update('request', match.params.id, { is_completed: !isCompleted })
                .then(res => {
                    setIsCompleteLoding(false)
                    setShowPop(false)
                    setReloadPage(!reloadPage)
                }
                ).catch(e => {
                    setIsCompleteLoding(false)
                    console.log(e)
                })
        }
    }

    const deleteT = () => {
        if (activeTransaction && activeTransaction.id) {
            setDeleteLoading(true)
            LModel.destroy(`${activeTransaction.birr ? 'credits' : 'debits'}`, activeTransaction.id)
                .then(res => {
                    setDeleteLoading(false)
                    // if (res && res.id) {
                    setShowPopDel(false)
                    setReloadPage(!reloadPage)
                    // }
                }).catch(e => {
                    setDeleteLoading(false)
                    console.log(e)
                })
        }

    }


    const PopupChild = () => {

        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">The transaction difference is
                    <span className="text-blue-600 font-semibold px-1">${MoneyFormat(Math.abs(totCredit - totDebit))}</span>birr, Do you want to save this transaction as compeleted ?</p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => markAsComplete()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{isCompleteLoading ? <CircularLoading loading={isCompleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )

    }




    const DeleteTransaction = ({ msg }) => {
        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want to delete {msg}

                    </p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPopDel(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => deleteT()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )
    }




    return (


        <div className="bg-white rounded p-4 h-full">

            <div className="m-2 p-1 flex justify-between">
                {exReq && exReq.client && exReq.client.name ?
                    <p className="text-center my-auto inline-block text-lg text-gray-800 font-thin"> <span className="mx-1 text-blue-700 font-semibold">{exReq.client.id ? exReq.client.name + "'s" : '--'}</span> Exchange request on <span className="mx-1 text-blue-700 font-semibold">{moment(exReq.name).isValid() ? moment(exReq.name).format("MMMM Do YY") : exReq.name}</span> </p>
                    : <DotLoading color="bg-gray-600" />
                }
                <div>
                    {exReq && exReq.is_supplier ?
                        <label className="bg-yellow-300 inline-block align-bottom text-gray-700 font-thin rounded-full p-2 px-6 mx-4 m-1">Supplier</label>
                        : null}
                    <button onClick={() => history.push('/dashboard/exrequest')} className="border rounded border border-gray-300 p-3 hover:bg-gray-500 hover:stroke-white stroke-gray-500 px-3 my-auto">
                        <X size={22} strokeWidth="4" />
                    </button>
                </div>
            </div>



            <div className="rounded-md w-full inline-flex mb-6">
                {exReq && exReq.is_supplier ?
                    <Fragment>
                        <div className="h-full w-1/2 m-2">
                            <DebitTransaction
                                setTotal={(val) => setTotDebit(val)}
                                reloadAllPage={reloadPage}
                                id={match && match.params && match.params.id ? match.params.id : null}
                                setActiveTransaction={(ct) => setActiveTransaction(ct)}
                                setShowPopDel={() => setShowPopDel(true)} />
                        </div>
                        <div className="h-full w-1/2 m-2">
                            <CreditTransaction
                                setTotal={(val) => setTotCredit(val)}
                                reloadAllPage={reloadPage}
                                // setReloadPage = {()=>setReloadPage(!reloadPage)}
                                id={match && match.params && match.params.id ? match.params.id : null}
                                setIsCompleted={(isc) => setIsCompleted(isc)}
                                setExReq={(ex) => setExReq(ex)}
                                setActiveTransaction={(ct) => setActiveTransaction(ct)}
                                setShowPopDel={() => setShowPopDel(true)} />
                        </div>
                    </Fragment>
                    :
                    <Fragment>
                        <div className="h-full w-1/2 m-2">
                            <CreditTransaction
                                setTotal={(val) => setTotCredit(val)}
                                reloadAllPage={reloadPage}
                                // setReloadPage = {()=>setReloadPage(!reloadPage)}
                                id={match && match.params && match.params.id ? match.params.id : null}
                                setIsCompleted={(isc) => setIsCompleted(isc)}
                                setExReq={(ex) => setExReq(ex)}
                                setActiveTransaction={(ct) => setActiveTransaction(ct)}
                                setShowPopDel={() => setShowPopDel(true)} />
                        </div>
                        <div className="h-full w-1/2 m-2">
                            <DebitTransaction
                                setTotal={(val) => setTotDebit(val)}
                                reloadAllPage={reloadPage}
                                id={match && match.params && match.params.id ? match.params.id : null}
                                setActiveTransaction={(ct) => setActiveTransaction(ct)}
                                setShowPopDel={() => setShowPopDel(true)} />
                        </div>
                    </Fragment>
                }
            </div>

            <div className="rounded-md border border-blue-500 flex mt-10 justify-around m-3">
                <div className="p-6 inline-flex w-full justify-around">
                    <p className="font-thin text-4xl text-gray-800">Difference</p>
                    <p className={`font-semibold text-3xl`}> {exReq && exReq.is_supplier ?
                        <span className={`${totCredit > totDebit ? 'text-red-600' : 'text-green-600'}`}> {MoneyFormat(totDebit - totCredit)} </span>
                        :
                        <span className={`${totCredit > totDebit ? 'text-green-600' : 'text-red-600'}`}>{MoneyFormat(totCredit - totDebit)} </span>}
                    </p>
                </div>
                {
                    isCompleted ?
                        <div onClick={() => validateCompeletion()} className="p-3 border-l text-center">
                            {
                                isCompleteLoading ?
                                    <CircularLoading loading={isCompleteLoading} color={'bg-gray-700'} />
                                    :
                                    <Fragment>
                                        <CheckSquare size={50} color="green" className="inline" />
                                        <p className="text-sm text-green-700">Completed</p>
                                    </Fragment>
                            }
                        </div>
                        :

                        <div onClick={() => validateCompeletion()} className="p-3 border-l text-center stroke-gray-500 hover:stroke-green">
                            {
                                isCompleteLoading ?
                                    <CircularLoading loading={isCompleteLoading} color={'bg-gray-700'} />
                                    :
                                    <Fragment>
                                        <Square size={50} className="inline" />
                                        <p className="text-sm text-gray-700">Incomplete</p>
                                    </Fragment>
                            }
                        </div>
                }
            </div>

            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<PopupChild />}
                parentCss={'w-5/12 bg-white rounded p-6'}

            />

         

        </div>


    )
}

export default ExRequestTransaction;

// msg={<Fragment><span className="text-blue-800 font-semibold italic mx-1"> {activeCTransaction && activeCTransaction.id ?
//     `${activeExReq.activeCTransaction.name}'s` :
//     '--'}

// </span> Exchange Request <span className="text-blue-800 font-semibold italic mx-1"> {moment(activeExReq && activeExReq.name ?
//     activeExReq.name :
//     null).isValid() ? moment(activeExReq.name).format("MMMM Do YY")
//     :
//     activeExReq && activeExReq.name ?
//         activeExReq.name
//         : ''}
//     </span> and its Transactions </Fragment>}