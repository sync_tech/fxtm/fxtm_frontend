import React, { useState, useEffect, Fragment } from 'react';
import { X, Square, CheckSquare, FolderPlus, Plus, Edit3, Trash2 } from 'react-feather';
import AutoCompleteSearch from '../../components/ui/auto_complete_search';
import Filter from '../../components/ui/filter';
import Loading from '../../components/loading/dot_loading';

import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading';
import moment from 'moment';
import { useHistory } from '../../providers/history_provider'
import NewLoanTransaction from './new_transaction'

import Popup from '../../components/ui/popup';
import { MoneyFormat } from '../../services/money_format';



const LoanTransaction = ({ match }) => {


    const [loading, setLoading] = useState(false)
    const [loan, setLoan] = useState({ loan: [] })
    // const [loanInfo, setLoanInfo] = useState({ previous: null, next: null, count: 0 })

    const [totCredit, setTotCredit] = useState(0)
    const [totDebit, setTotDebit] = useState(0)
    const [isCompleteLoading, setIsCompleteLoding] = useState(false)
    const { history } = useHistory()

    // const [toogleNew, setToogleNew] = useState(false)
    const [reload, setReload] = useState()
    const [totalTransaction, setTotalTransaction] = useState(0)

    const [showpop, setShowPop] = useState(false)



    const [loadingPaidLoan, setLoadingPaidLoan] = useState()
    const [paidLoan, setPaidLoan] = useState()
    const [toogleNewPL, setToogleNewPL] = useState(false)
    const [totalPaidLoan, setTotalPaidLoan] = useState(0)

    const [loadingTakenLoan, setLoadingTakenLoan] = useState()
    const [takenLoan, setTakenLoan] = useState()
    const [toogleNewTL, setToogleNewTL] = useState(false)
    const [totalTakenLoan, setTotalTakenLoan] = useState(0)

    const [activeTransaction, setActiveTransaction] = useState()

    const loadLoan = () => {

        if (match && match.params && match.params.id) {

            const reducer = (acc, cVal) => acc + cVal;

            setLoading(true)
            // LModel.find('loanTransaction', match.params.id, filter !== null ? `${filter}&name=${searchKey}&page=${1}` : `&name=${searchKey}&page=${paginationIndex}`)
            LModel.find('loanTransaction', match.params.id, null)
                .then(res => {
                    setLoading(false)
                    if (res) {
                        setLoan(res)

                        let transaction_tot = res && res.loan && res.loan.length > 0 ?
                            res.loan.map(l => parseFloat(l.repayment)).reduce(reducer) : null
                        isNaN(transaction_tot) ? setTotalTransaction(0) : setTotalTransaction(transaction_tot)


                    }
                }).catch(e => {
                    setLoading(false)
                    console.log(e)
                })
        }
    }

    const loadPaidLoanTransaction = () => {
        if (match && match.params && match.params.id) {

            const paid_reducer = (acc, cVal) => acc + cVal;

            setLoadingPaidLoan(true)
            LModel.find('loan_paid_ledger', match.params.id, null)
                .then(res => {
                    setLoadingPaidLoan(false)
                    if (res) {
                        setPaidLoan(res)

                        let total_paid_loan = res && res.loan && res.loan.length > 0 ?
                            res.loan.map(l => parseFloat(l.amount)).reduce(paid_reducer) : null
                        isNaN(total_paid_loan) ? setTotalPaidLoan(0) : setTotalPaidLoan(total_paid_loan)


                        let ln = {...res}
                        ln["loan"] = []
                        setLoan(ln)
                    }
                }).catch(e => {
                    setLoadingPaidLoan(false)
                    console.log(e)
                })
        }
    }

    const loadTakenLoanTransaction = () => {
        if (match && match.params && match.params.id) {

            const taken_reducer = (acc, cVal) => acc + cVal;

            setLoadingTakenLoan(true)
            LModel.find('loan_taken_ledger', match.params.id, null)
                .then(res => {
                    setLoadingTakenLoan(false)
                    if (res) {
                        setTakenLoan(res)

                        let total_taken_loan = res && res.loan && res.loan.length > 0 ?
                            res.loan.map(l => parseFloat(l.amount)).reduce(taken_reducer) : null
                        isNaN(total_taken_loan) ? setTotalTakenLoan(0) : setTotalTakenLoan(total_taken_loan)


                    }
                }).catch(e => {
                    setLoadingTakenLoan(false)
                    console.log(e)
                })
        }
    }


    useEffect(() => {
        // loadLoan()
        loadPaidLoanTransaction()
        loadTakenLoanTransaction()
    }, [reload])


    useEffect(() => {
        if (activeTransaction && activeTransaction.id && activeTransaction["loan_type"]) {
            setToogleNewPL(true)
            setToogleNewTL(false)
            if (showpop) {
                setToogleNewPL(false)
                setToogleNewTL(false)

            }
        } else if (activeTransaction && activeTransaction.id) {
            setToogleNewTL(true)
            setToogleNewPL(false)

            if (showpop) {
                setToogleNewTL(false)
                setToogleNewPL(false)

            }
        }

    }, [activeTransaction])



    // useEffect(() => {
    //     // if (activeTransaction && activeTransaction.id) {
    //     //     setToogleNewTL(true)
    //     // }
    //     // if (showpop) {
    //     //     setToogleNewTL(false)
    //     // }
    //     console.log("/FFFFF")
    // }, [activeTransaction && (activeTransaction["loan_type"] === false)])


    const markAsComplete = () => {

        if (loan && loan.id) {

            setIsCompleteLoding(true)
            LModel.update('loan', loan.id, { is_completed: !(loan['is_completed']) })
                .then(res => {
                    setIsCompleteLoding(false)
                    // setShowPop(false)
                    setReload(!reload)
                }
                ).catch(e => {
                    setIsCompleteLoding(false)
                    console.log(e)
                })

        }
    }

    const PopupChild = () => {

        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                {/* <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">The transaction difference is
                    <span className="text-blue-600 font-semibold px-1">${MoneyFormat(Math.abs(totCredit - totDebit))}</span>birr, Do you want to save this transaction as compeleted ?</p>
                </div> */}
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want to delete this loan transaction {}
                        {/* <span className="text-blue-600 font-semibold px-1">${MoneyFormat(Math.abs(totCredit - totDebit))}</span>birr, Do you want to save this transaction as compeleted ? */}
                    </p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => markAsComplete()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{isCompleteLoading ? <CircularLoading loading={isCompleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )

    }

    const Loan = () => {
        return (
            <div className="text-center rounded border-2 w-1/3 m-auto">
                <h2 className="text-gray-700 font-semibold text-2xl p-2 capitalize">{loan.name}</h2>
                {/* <p className="text-gray-700 text-center p-2 py-1">{loan.reason}</p> */}
                <div className="flex justify-center p-2">
                    <p className="text-red-600 text-lg">{MoneyFormat(totalTakenLoan)}</p>
                    <div className="w-1 rounded bg-gray-500 mx-4"></div>
                    <p className="text-green-600 text-lg">{MoneyFormat(totalPaidLoan)}</p>
                </div>
                <div className="text-center">
                    <p className="text-gray-700 text-3xl font-semibold p-1">
                        {MoneyFormat((totalTakenLoan - totalPaidLoan))}
                    </p>
                </div>
                <div className="text-left">
                    {
                        loan && loan['is_completed'] ?
                            <div onClick={() => markAsComplete()} className="m-1 p-2 text-center inline-flex items-center rounded hover:bg-blue-100">
                                {
                                    isCompleteLoading ?
                                        <CircularLoading loading={isCompleteLoading} color={'bg-gray-700'} />
                                        :

                                        <Fragment>
                                            <CheckSquare size={22} color="green" />
                                            <p className="text-gray-700 pl-3 font-xl">Closed </p>
                                        </Fragment>

                                }
                            </div>
                            :

                            <div onClick={() => markAsComplete()} className="m-1 p-2 items-center stroke-gray-500 inline-flex hover:text-green-700 rounded hover:bg-blue-100">
                                {
                                    isCompleteLoading ?
                                        <CircularLoading loading={isCompleteLoading} color={'bg-gray-700'} />
                                        :
                                        <Fragment>
                                            <Square size={22} />
                                            <p className="text-gray-700 pl-3 font-xl">Close this loan </p>
                                        </Fragment>

                                }
                            </div>
                    }

                </div>

            </div>

        )
    }


    const LoanList = ({ loading, data, toogleNew, total, is_paid, setToogleNew }) => {


        return (
            <Fragment>
                {
                    loading ?
                        <Loading loading={loading} color={'bg-gray-600'} />
                        :
                        data && data["loan"] && data["loan"].length > 0 ?
                            <Fragment>
                                {!toogleNew && data && !data.is_completed?
                                    <button onClick={() => {
                                        setActiveTransaction()
                                        setToogleNew(true)
                                    }} className={`border rounded-md ${is_paid ? 'float-right' : 'float-left'}  border-gray-600 p-2 my-3 hover:bg-green-700 hover:stroke-white stroke-green px-3`}>
                                        <Plus size={22} strokeWidth="4" className="mr-1 inline" /> ADD NEW </button> : null}
                                <table className="w-full bg-blue-100">
                                    <thead className="bg-blue-700">
                                        <tr className="border p-2 text-white">
                                            <th className="border-r border-gray-400 p-2 text-center">#</th>
                                            <th className="border-r border-gray-400 font-semibold text-center">Date</th>
                                            <th className="border-r border-gray-400 font-semibold text-center">Amount</th>
                                            <th className="border-r border-gray-400 font-semibold text-center">Remind</th>
                                            <th className="border-r border-gray-400 font-semibold text-center">Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody className="">
                                        {
                                            data["loan"].map((pl, index) => (
                                                <tr key={index} className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                                    <td className="border-r p-2 text-sm">{index + 1}</td>
                                                    <td className="border-r text-gray-900 text-sm">{moment(pl.created_at).format('ll')}</td>
                                                    <td className="border-r text-gray-900 text-sm">{pl.amount ? MoneyFormat(pl.amount) : '--'}</td>
                                                    <td className="border-r text-gray-900 text-sm">{pl.name ? pl.name : '--'}</td>
                                                    <td className="border-r text-gray-900 text-sm">
                                                        <div className="inline-flex">
                                                            <p onClick={() => {
                                                                setActiveTransaction(pl)
                                                                setToogleNew(true)
                                                            }} className="text-gray-700 rounded p-1  hover:text-white hover:bg-blue-400"> <Edit3 size={18} className={"inline"} /> </p>
                                                            <p onClick={() => {
                                                                setActiveTransaction(pl)
                                                                setShowPop(true)
                                                            }} className="text-gray-700 rounded p-1 hover:text-white hover:bg-red-400"> <Trash2 size={18} className={"inline"} /> </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                        <tr className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                            <td className="border-r p-2 text-sm">{data["loan"].length + 1}</td>
                                            <td className="border-r text-gray-900 text-sm">{}</td>
                                            <td className="border-r text-gray-900 text-sm">{}</td>
                                            <td className="border-r text-gray-900 text-sm">{}</td>
                                            <td className="border-r text-gray-900 text-sm">{}</td>
                                        </tr>
                                        <tr className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                            <td colSpan="2" className="border-r text-gray-900 p-2 text-lg">{'Total'}</td>
                                            <td colSpan="3" className="border-r text-green-800 font-semibold text-lg">{MoneyFormat(total)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Fragment>
                            :
                            <div className="w-full text-center p-4">
                                <div className="">
                                    <p>
                                        <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-teal-100" size={140} color={'gray'} strokeWidth={0.5} />
                                    </p>
                                    {is_paid ?
                                        <em> paid loan list is empty.</em>
                                        :
                                        <em> taken loan list is empty.</em>
                                    }
                                </div>
                            </div>
                }

                {
                    toogleNew ?
                        <NewLoanTransaction
                            closeSave={() => setToogleNew(false)}
                            is_paid={is_paid}
                            loan={data}
                            activeT={activeTransaction}
                            reloadPage={() => setReload(!reload)} /> : null

                }

            </Fragment>
        )

    }



    return (


        <div className="bg-white rounded p-4 h-full">

            <div className="flex justify-end p-4">
                {/* <AutoCompleteSearch /> */}
                {/* <div className="inline-flex">
                        <button onClick={() => setToggleNewRequest(!toggleNewRequest)} className="border rounded-l border border-gray-600 p-2 my-auto hover:bg-green-700 hover:stroke-white stroke-green px-3 my-auto">
                            <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                            New Request
                        </button>
                        <Filter />
                    </div> */}

                {/* {!toogleNewPL && loan && !loan.is_completed ?
                    <button onClick={() => setToogleNewPL(true)} className="border rounded-md float-right border-gray-600 p-2 mx-3 hover:bg-green-700 hover:stroke-white stroke-green px-3">
                        <Plus size={22} strokeWidth="4" className="mr-1 inline" />ADD NEW </button>
                    : null} */}

                <button onClick={() => history.push('/dashboard/loan')} className="border rounded border border-gray-300 p-3 my-auto hover:bg-gray-500 hover:stroke-white stroke-gray-500 px-3 my-auto">
                    <X size={22} strokeWidth="4" />
                </button>
            </div>

            <div className="rounded-md w-full inline-flex mb-6">
                {/* <div className="h-full w-1/3 m-2">
                    <div className="w-full rounded border border-blue-500">
                        <Loan />
                    </div>
                </div>
                <div className="h-full w-2/3 m-2">

                    {
                        loading ?
                            <Loading loading={loading} color={'bg-gray-600'} />
                            :
                            loan && loan.loan.length > 0 ?
                                <Fragment>

                                    <table className="w-full bg-gray-100">
                                        <thead className="bg-blue-700">
                                            <tr className="border p-2 text-white">
                                                <th className="border-r border-gray-400 p-2 text-center">#</th>
                                                <th className="border-r border-gray-400 font-semibold text-center">Note</th>
                                                <th className="border-r border-gray-400 font-semibold text-center">Repayment Amount</th>
                                                <th className="border-r border-gray-400 font-semibold text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody className="">
                                            {
                                                loan.loan.map((deb, index) => (
                                                    <tr key={index} className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                                        <td className="border-r p-2 text-sm">{index + 1}</td>
                                                        <td className="border-r text-gray-900 text-sm">{deb.name ? deb.name : '--'}</td>
                                                        <td className="border-r text-gray-900 text-sm">{deb.repayment ? MoneyFormat(deb.repayment) : '--'}</td>
                                                        <td className="border-r text-gray-900 text-sm">{moment(deb.created_at).format('ll')}</td>
                                                    </tr>
                                                ))
                                            }
                                            <tr className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                                <td className="border-r p-2 text-sm">{loan && loan.loan.length + 1}</td>
                                                <td className="border-r text-gray-900 text-sm">{}</td>
                                                <td className="border-r text-gray-900 text-sm">{}</td>
                                                <td className="border-r text-gray-900 text-sm">{}</td>
                                            </tr>
                                            <tr className={`border cursor-pointer hover:bg-gray-300 text-center`}>
                                                <td colSpan="2" className="border-r text-gray-900 p-2 text-lg">{'Total'}</td>
                                                <td colSpan="3" className="border-r text-green-800 font-semibold text-lg">{MoneyFormat(totalTransaction)}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </Fragment>
                                :
                                <div className="w-full text-center p-4">
                                    <div className="">
                                        <p>
                                            <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-teal-100" size={140} color={'gray'} strokeWidth={0.5} />
                                        </p>
                                        <em>returned payment list is empty.</em>
                                    </div>
                                </div>
                    }

                    {
                        toogleNew ?
                            <NewLoanTransaction closeSave={() => setToogleNew(!toogleNew)} loanId={loan.id} reloadPage={() => setReload(!reload)} /> : null
                    }





                </div> */}




                <div className="h-full w-1/2 m-2">
                    <LoanList loading={loadingTakenLoan} data={takenLoan} toogleNew={toogleNewTL} total={totalTakenLoan} is_paid={false} setToogleNew={(v) => setToogleNewTL(v)} />
                </div>
                <div className="h-full w-1/2 m-2">
                    <LoanList loading={loadingPaidLoan} data={paidLoan} toogleNew={toogleNewPL} total={totalPaidLoan} is_paid={true} setToogleNew={(v) => setToogleNewPL(v)} />
                </div>


            </div>
            <Loan />


            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<PopupChild />}
                parentCss={'w-5/12 bg-white rounded p-6'}

            />

        </div>


    )
}

export default LoanTransaction;

