import React, { useState, useEffect, Fragment } from 'react';
import LModel from '../../services/api';
import CurrencyLoading from '../../components/loading/currency_loading';
import { Folder } from 'react-feather'

const CurrenciesCard = ({ setParentActiveCurrency }) => {

    const [currencies, setCurrencies] = useState()
    const [currencyLoad, setCurrencyLoad] = useState(false)
    const [activeCurrency, setActiveCurrency] = useState()

    const loadCurrency = () => {
        setCurrencyLoad(true)
        LModel.find('currencies')
            .then(res => {
                // if (res && res.results) {
                setCurrencyLoad(false)
                setActiveCurrency(res[0])
                setCurrencies(res)
                // setActiveIdx(0)
                // setCTransaction(res.results)
                // }
            }).catch(e => {
                setCurrencyLoad(false)
                console.log(e)
            })
    }

    useEffect(() => {
        loadCurrency()
    }, [])


    useEffect(() => {
        setParentActiveCurrency(activeCurrency)
    }, [activeCurrency])




    return (
        <Fragment>

            {
                currencyLoad ?
                    <div className="flex justify-center">
                        <CurrencyLoading />
                    </div >
                    :
                    currencies && currencies.length > 0 ?

                        <div className="bg-gray-400 mr-3 w-2/12 rounded-sm border p-1 overflow-y-auto" style={{ height: "calc(95vh - 100px)" }}>
                            {
                                currencies.map((crncy, index) => (
                                    crncy.code === 'ETB' ?
                                     null 
                                     :
                                    <div key={index} onClick={() => setActiveCurrency(crncy)} className={`bg-white rounded-md hover:shadow-lg p-2 mb-3 ${crncy === activeCurrency ? 'bg-blue-200 border border-white' : 'border-4 border-white hover:border-blue-400'} `}>
                                        <div className="flex justify-between px-2">
                                            <p className="text-green-600 font-semibold">{crncy.rate}</p>
                                            <p className="text-gray-700 italic font-thin">#4</p>
                                        </div>
                                        <p className="text-3xl font-semibold text-center rounded  text-gray-700 hover:text-gray-800">43k</p>
                                        {/* <div className="flex">
                                            
                                            <p className="text-lg text-center font-thin text-gray-700">{crncy.code}</p>
                                        </div> */}
                                        <div className="flex justify-between px-2">
                                            <img src={crncy.flag} className="w-5 h-4 my-auto mx-1" />
                                            <p className="text-lg text-center font-thin text-gray-700">{crncy.code}</p>
                                        </div>
                                    </div>

                                ))
                            }
                        </div>
                        :
                        <div className="text-center p-4">

                            <div className="">
                                {/* <p className={searchKey === undefined ? 'hidden' : 'block text-gray-800 text-2xl font-thin'}> {`Not Found ${searchKey}`}</p> */}
                                <p>
                                    <Folder onClick={() => console.log(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-white" size={140} color={'gray'} strokeWidth={0.5} />
                                </p>
                                <em>Currency list is Empty.</em>

                            </div>

                        </div>

            }
        </Fragment>
    )


}

export default CurrenciesCard;