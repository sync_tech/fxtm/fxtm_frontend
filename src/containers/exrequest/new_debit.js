import React, { useState, useEffect } from 'react';
import AutoCompleteDropdown from '../../components/ui/auto_complete_dropdown';
import CustomField from '../../components/form/custom_field';
import LModel from '../../services/api';
import Loading from '../../components/loading/dot_loading';
import CircularLoading from '../../components/loading/circular_loading';

const NewDebit = ({ toogleNq, exReq, reloadPage, activeD }) => {


    const [newDebit, setNewDebit] = useState({
        id: activeD && activeD.id ? activeD["id"] : "",
        exRequest: exReq,
        to: activeD && activeD.to ? activeD["to"] : " ",
        amount: activeD && activeD.amount ? activeD["amount"] : ""
    })
    const [loading, setLoading] = useState(false)
    const [bank, setBank] = useState()




    function handleInputChange(ky, val) {
        let newD = { ...newDebit }
        newD[ky] = val
        setNewDebit(newD)
    }


    const saveRequest = () => {
        if (newDebit && newDebit.amount && newDebit.id) {
            setLoading(true)
            LModel.update('debits', newDebit.id, newDebit)
                .then(res => {
                    setLoading(false)
                    // setRequest(res)
                    toogleNq()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    toogleNq()
                })
        } else if (newDebit && newDebit.amount) {

            setLoading(true)
            LModel.create('debits/', newDebit)
                .then(res => {
                    setLoading(false)
                    // setRequest(res)
                    toogleNq()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    toogleNq()
                })
        }
    }



    return (
        <div className="rounded border border-2 border-blue-900 flex flex-col p-2 my-2">

            <div className="flex m-2">

                <CustomField
                    name="to"
                    placeholder=""
                    type="text"
                    onBlur={() => console.log()}
                    label="To"
                    defaultValue={newDebit["to"]}
                    onChange={e => handleInputChange('to', e)}
                    divStyle="w-3/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />
                {/* <CustomField
                    name="bank"
                    placeholder=""
                    type="text"
                    onBlur={() => console.log()}
                    label="Bank"
                    onChange={e => handleInputChange('bank', e)}
                    divStyle="w-3/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                /> */}
                <AutoCompleteDropdown
                    label={'bank'}
                    divStyle={'w-3/12 px-1 mb-6 md:mb-0'}
                    field={'name'}
                    url={'bank'}
                    placeHolder={'Cash'}
                    onValueSet={(vl) => vl && vl.id ? handleInputChange('bank', vl.id) : handleInputChange('bank', null)}
                />
                <CustomField
                    name="amount"
                    placeholder="birr"
                    type="number"
                    onBlur={() => console.log()}
                    label="Birr"
                    defaultValue={newDebit["amount"]}
                    rules={{ required: true, min: 1 }}
                    onChange={e => handleInputChange('amount', e)}
                    divStyle="w-6/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />
            </div>


            <div className="w-full text-right my-4 px-8">
                <button onClick={() => saveRequest()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{loading ? <CircularLoading loading={loading} /> : newDebit && newDebit.id ? "Update" :"Save"}</button>
                <button onClick={() => toogleNq()} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
            </div>

        </div>
    )
}

export default NewDebit;
