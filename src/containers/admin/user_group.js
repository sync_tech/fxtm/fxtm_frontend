import React, { useState, useEffect, Fragment } from 'react'
import CircularLoading from '../../components/loading/circular_loading';
import LModel from '../../services/api';
import { Check, CheckCircle } from 'react-feather'

export default ({ setShowPop, usr }) => {

    // const [deleteLoading, setDeleteLoading] = useState()
    const [usrG, setUsrG] = useState(usr)
    const [saveLoading, setSaveLoading] = useState(false)

    // const [loadUserData, setLoadUserData] = useState(false)

    // // const [loading, setLoading] = useState(false)

    const [usrGroup, setUsrGroup] = useState(usrG.usr_groups.map(g => g.name))




    // const [saveLoading, setSaveLoading] = useState(false)
    // const [toggle, setToggle] = useState(false)

    // 0: {id: 1, name: "root", permissions: []}
    // 1: {id: 2, name: "admin", permissions: []}


    useEffect(()=>{
        setUsrGroup(usrG.usr_groups.map(g => g.name))
    },[usrG])


    const saveGroup = (grp) => {

        if (usrG && usrG.id && grp) {
            setSaveLoading(true)

            LModel.update('manage-user', usrG.id, { groups: grp })
                .then(res => {
                    setSaveLoading(false)
                    setUsrG(res)
                    // setRequest(res)
                    // toogleNq()
                    // reloadPage()
                    // loadUsr()

                }
                ).catch(e => {
                    setSaveLoading(false)
                    console.log(e)
                    // setShowPop()
                })

        }

    }



    function AddGroup(id){

        let grp = usrG.groups ? usrG.groups : []
        grp.push(id)
        saveGroup(grp)
    }



    function RemoveGroup(id){

        let grp = usrG.groups ? usrG.groups : []
        
        let idx = grp.indexOf(id)
        grp.splice(idx, 1);

        saveGroup(grp)
    }





    return (
        <div className="w-full">
            <div className="text-center border-b-2 px-4">
                <p className="text-lg font-semibold m-1 text-orange-600">Manage User Group</p>
            </div>
            <div className=" flex flex-col p-2 mt-4  rounded">


                <div className="rounded p-2 flex">
                    <h3 className="text text-gray-700"> First Name </h3>
                    <h3 className="text-lg text-gray-700 font-semibold ml-4"> {usrG && usrG.first_name ? usrG.first_name : '--'} </h3>
                </div>
                <div className="rounded p-2 flex">
                    <h3 className="text text-gray-700"> Last Name </h3>
                    <h3 className="text-lg text-gray-700 font-semibold ml-4"> {usrG && usrG.last_name ? usrG.last_name : '--'} </h3>
                </div>
                <div className="rounded p-2 flex">
                    <h3 className="text text-gray-700"> User Name </h3>
                    <h3 className="text-lg text-gray-700 font-semibold ml-4"> {usrG && usrG.username ? usrG.username : '--'} </h3>
                </div>
                <div className="rounded p-2 flex">
                    <h3 className="text text-gray-700"> Groups </h3>
                    <h3 className="text-lg text-gray-700 font-semibold ml-4"> {usrG && usrG.usr_groups ? usrG.usr_groups.map(f => <p key={f.name} className="inline"> {f.name} </p>) : '--'} </h3>
                </div>



                <div className="rounded flex my-4 justify-center">
                    {
                        usrGroup && usrGroup.length > 0 && usrGroup.includes("admin") ?
                            <p onClick={() => RemoveGroup(1)} className="rounded-md border border-green-600 text-white m-2 px-4 p-3 bg-green-700 relative">Admin
                            <CheckCircle size={18} className="absolute top-0 right-0 bg-whie" />
                            </p>
                            :
                            <p onClick={() => AddGroup(1)} className="rounded-md border border-blue-800 text-gray-700 m-2 px-4 p-3 ">{saveLoading ? <CircularLoading loading={saveLoading} color={"bg-gray-600"}/> : "Admin"}
                            </p>
                    }
                    {
                        usrGroup && usrGroup.length > 0 && usrGroup.includes("personnel") ?
                            <p onClick={() => RemoveGroup(2)} className="rounded-md border border-green-600 text-white m-2 px-4 p-3 bg-green-700 relative">Personnel
                            <CheckCircle size={18} className="absolute top-0 right-0 bg-whie" />
                            </p>
                            :
                            <p onClick={() => AddGroup(2)}  className="rounded-md border border-blue-800 text-gray-700 m-2 px-4 p-3">{saveLoading ? <CircularLoading loading={saveLoading} color={"bg-gray-600"}/> : "Personnel"}
                            </p>
                    }

                </div>



            </div>

            <div className="w-full text-right">
                <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">{saveLoading ? <CircularLoading loading={saveLoading} /> : "Cancel"}</button>
                {/* <button onClick={() => saveGroup()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}</button> */}
            </div>
        </div>
    )
}
