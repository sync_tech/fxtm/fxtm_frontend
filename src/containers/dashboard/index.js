import React, { useState } from 'react';
import { Route } from 'react-router-dom';
import { dashboardRoutes } from '../../routes';
import PrivateRoute from '../../components/route/private_route';
import SideNav from '../nav/side_nav';

export default (props) => {
    const [toggleSideBar, setToggleSideBar] = useState(false)

    return (
        <div className="bg-dashboard min-h-screen w-screen">
            {/* <h1 className="text-2lg"> Dashboard </h1> */}
            <div className="">
                <SideNav path={props && props.location && props.location.pathname ? props.location.pathname : null} toggleSideBar={toggleSideBar} setToggleSideBar={(e) => setToggleSideBar(e)} />

                <div className={`relative ${toggleSideBar ? 'ml-20' : 'ml-56'} `}>
                    {/* md:ml-20 lg:ml-56 */}

                    {
                        dashboardRoutes.map((route, index) =>
                            route.private ? <PrivateRoute key={index} {...route} /> : <Route key={index} {...route} />
                        )
                    }

                </div>

            </div>
        </div>
    )
}