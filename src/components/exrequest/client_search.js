
import React from 'react';
import CircularLoading from '../../components/loading/circular_loading'
import { Search } from 'react-feather'


export default ({ setSearchKey, searchKey, circularloading, loadSearchResult}) => {



    const handlingSearchButtonClick = () => {
        if (searchKey != null && searchKey.length > 1) {
            loadSearchResult()
        }
    }

    const handlingSearchInputChange = (e) => {

        if (e.target.value != null && e.target.value.trim().length > 1) {
            setSearchKey(e.target.value)

        } else if (e.target.value === "") {
            setSearchKey("")
        } else {
            setSearchKey(e.target.value.trim())
        }

    }


    return (
        <div className="w-5/12">
            <div className="text-center relative mx-auto text-gray-600">
                <input className="w-full relative border-2 border-gray-300 bg-white h-12 px-5 pr-16 rounded-lg text-gray-700 focus:outline-none shadow-md"
                    autoComplete="off" type="search" name="search" placeholder="Search client "
                    onChange={handlingSearchInputChange}
                    value={searchKey} />
                <button type="submit" onClick={handlingSearchButtonClick} className="absolute right-0 top-0 mt-3 mr-4">
                    {
                        circularloading ?
                            <CircularLoading loading={circularloading} color={"bg-gray-600"} />
                            :
                            <Search size={20} strokeWidth={3} />
                    }
                </button>
            </div>
        </div>

    )
}
