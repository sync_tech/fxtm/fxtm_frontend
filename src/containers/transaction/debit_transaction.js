import React, { useState, useEffect, Fragment } from 'react';
import { Folder, ChevronDown } from 'react-feather';
import AutoCompleteSearch from '../../components/exrequest/client_search';
import Filter from '../../components/ui/filter';
import LModel from '../../services/api';
import Loading from '../../components/loading/dot_loading';
import moment from 'moment';
import TableLoading from '../../components/loading/table_loading';
import Pagination from '../../components/ui/pagination'
import { MoneyFormat } from '../../services/money_format';


const DebitTransaction = () => {

    const [dtransaction, setDTransaction] = useState([])
    const [dtransactionInfo, setDtransactionInfo] = useState({ previous: null, next: null, count: 0 })

    const [loading, setLoading] = useState(false)

    const [reload, setReload] = useState(false)
    const [searchKey, setSearchKey] = useState("")
    const [circularloading, setCircularloading] = useState(false)

    const [filter, setFilter] = useState({ initialDate: '', finalDate: '' })
    const [paginationIndex, setPaginationIndex] = useState(1)



    const loadDTransaction = () => {

        setLoading(true)
        LModel.find('debitLedger', null, filter && filter.initialDate ? `created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${1}` : `created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}&page=${paginationIndex}`)
            .then(res => {
                if (res && res.results) {
                    setLoading(false)
                    setDTransaction(res.results)
                    setDtransactionInfo(res)
                }
            }).catch(e => {
                setLoading(false)
                console.log(e)
            })
    }


    const loadSearchResult = () => {
        setCircularloading(true)
        LModel.find(`debitLedger`, null, `exRequest__client__name=${searchKey}&created_at__gte=${filter.initialDate}&created_at__lte=${filter.finalDate}`)

            .then(res => {
                if (res && res.results) {
                    setDtransactionInfo(res)
                    setDTransaction(res.results)
                    setCircularloading(false)
                }
            }).catch(err => {
                console.log(err)
                setCircularloading(false)
            })

    }


    // useEffect(() => {
    //     loadDTransaction()
    // }, [])


    useEffect(() => {
        if (searchKey != null && searchKey.trim().length > 1) {
            loadSearchResult()
        }
        else {
            loadDTransaction()
        }
    }, [searchKey, reload, filter, paginationIndex])



    return (

        <Fragment>


            <div className="flex justify-between">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadSearchResult}
                />
                <div className="inline-flex">
                    <Filter filter={filter} setFilter={setFilter} />
                </div>
            </div>


            {
                loading ?
                    <div className="flex justify-center">
                        <TableLoading />
                    </div>
                    :
                    dtransaction && dtransaction.length > 0 ?
                        <table className="w-full mt-8 bg-white">
                            <thead className="bg-blue-700">
                                <tr className="border p-2 text-white">
                                    <th className="border-r border-gray-400 p-2 text-center">#</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Client</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">To</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Bank</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Birr</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Transactor<ChevronDown className="inline float-right hover:bg-blue-800 m-1" size={18} /></th>
                                    <th className="border-r border-gray-400 font-semibold text-center">On</th>
                                    {/* <th className="border-r border-gray-400 pr-2"><FontAwesomeIcon icon={faPlusCircle} onClick={() => history.push('/dashboard/vehicles/new')} title="Add New Vehicle" className="text-4xl float-right hover:text-blue-800 text-blue-500 hover:shadow-outline border rounded-full h-7 w-7" /></th> */}
                                </tr>
                            </thead>
                            <tbody className="">
                                {
                                    dtransaction.map((tran, index) => (
                                        <tr key={index} className={`border cursor-pointer hover:bg-gray-200 text-center`}>
                                            <td className="border-r p-2 text-sm">{filter && filter.initialDate ? index + 1 : index + 1 + ((paginationIndex - 1) * 20)}</td>
                                            <td className="border-r text-gray-900 text-sm">{tran && tran.exRequest && tran.exRequest.client && tran.exRequest.client.name ? tran.exRequest.client.name : '--'}</td>
                                            <td className="border-r text-gray-900 text-sm">{tran.to}</td>
                                            <td className="border-r text-gray-900 text-sm">{tran && tran.bank && tran.bank.id ? tran.bank.name : "Cash"}</td>
                                            <td className="border-r text-gray-900 font-semibold text-sm">{MoneyFormat(tran.amount)}</td>
                                            <td className="border-r text-gray-900 text-sm">{'Jon'}</td>
                                            <td className="border-r p-2 flex justify-evenly">
                                                {/* <Link to={`vehicles/detail/${v.id}`} className={`active:outline-none p-2 bg-transparent rounded hover:text-white hover:bg-blue-500 text-blue-700 px-4 mr-2`} title={`Click to view detail`}>More...</Link> &nbsp; */}
                                                {/* <Link to={`vehicles/edit/${v.id}`} className={`active:outline-none p-2 bg-transparent rounded hover:text-white focus:shadow-outline hover:bg-blue-500 text-blue-700 px-4`} title={`Click to edit`}>Edit</Link> */}
                                                <p className="my-auto text-sm text-gray-900">{moment(tran.created_at).format('ll')}</p>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                        :
                        <div className="w-full text-center p-10">

                            <div className="">
                                <p>
                                    <Folder className="inline-flex text-center rounded-lg m-2" size={140} color={'gray'} strokeWidth={0.5} />
                                </p>
                                <em>Transaction under <span className="italic">{searchKey}</span> is not found.</em>
                            </div>
                        </div>
            }


            <Pagination
                count={dtransactionInfo.count}
                previous={dtransactionInfo.previous}
                next={dtransactionInfo.next}
                list={20}
                activeIndex={paginationIndex}
                setIndex={(i) => setPaginationIndex(i)}
            />
        </Fragment>


    )
}

export default DebitTransaction;


// Edit && delete(soft)
// user admin
// dashboard && report
// comments
// resourece management
// date and time picker
// onsucess reply ok
// notifiy internet disconnect