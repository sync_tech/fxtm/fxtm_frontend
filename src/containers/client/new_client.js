import React, { useState, Fragment } from 'react';
import CircularLoading from '../../components/loading/circular_loading'
import CustomField from '../../components/form/custom_field';
import { Plus, Minus } from 'react-feather'
import LModel from '../../services/api';


export default ({ toogleC, reload, activeClient }) => {

    const [loading, setLoading] = useState(false)
    const [newClient, setNewClient] = useState( activeClient ? activeClient : { name: "", phone: [] })



    function handleInputChange(ky, vl) {
        let c_name = { ...newClient }
        c_name[ky] = ky === 'phone' ? [vl] : vl
        setNewClient(c_name)
    }

    const saveClient = () => {


        if (newClient && newClient.id && newClient.name) {

            setLoading(true)
            LModel.update('clients', newClient.id, newClient)
                .then(res => {
                    setLoading(false)
                    // setRequest(res)
                    toogleC()
                    reload()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    toogleC()
                })

        } else if (newClient && newClient.name) {

            setLoading(true)
            LModel.create('clients/', newClient)
                .then(res => {
                    setLoading(false)
                    // setRequest(res)
                    toogleC()
                    reload()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    toogleC()
                })

        }
    }


    return (
        <div className="m-4 rounded border border-blue-500 p-2">
            <div className="flex m-2 justify-center">
                <CustomField
                    name="name"
                    placeholder=""
                    type="text"
                    defaultValue={newClient["name"]}
                    onBlur={() => console.log()}
                    label="Client Name"
                    onChange={e => handleInputChange('name', e)}
                    divStyle="w-6/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />
            </div>

            <div className="flex m-2 justify-center">

                <CustomField
                    name="phone"
                    placeholder="Phone #1"
                    type="text"
                    label=""
                    defaultValue={newClient && newClient.phone ? newClient.phone[0]:""}
                    onBlur={() => console.log()}
                    onChange={e => handleInputChange('phone', e)}
                    divStyle="w-6/12 px-1 mb-6 md:mb-0"
                    labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                    inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border rounded border-gray-200  py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                />



            </div>

            <div className="w-full text-center my-2">
                <button onClick={() => saveClient()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{loading ? <CircularLoading loading={loading} /> : newClient.id ? "Update" : "Save"}</button>
                <button onClick={() => toogleC()} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
            </div>

        </div>
    )
}