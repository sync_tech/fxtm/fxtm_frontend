import React, { useState, useEffect, Fragment, useRef } from 'react';
import LModel from '../../services/api';
import CurrencyLoading from '../../components/loading/currency_loading';
import { FolderPlus, Plus, ChevronDown, Edit, Trash2, MoreVertical } from 'react-feather'
import AccountCard from "../../components/account/bank_account_card";
import CircularLoading from '../../components/loading/circular_loading';
import Popup from '../../components/ui/popup';
import CustomField from '../../components/form/custom_field';
import NewAccount from '../../containers/accounts/new_bank_account';

const BankSelection = ({ setParentActiveCurrency, reloadParentPage, parentProp }) => {

    const [accounts, setAccounts] = useState()
    const [accountLoad, setAccountLoad] = useState(false)
    const [activeAccount, setActiveAccount] = useState()
    const [bank, setBank] = useState([])
    const [bankLoad, setBankLoad] = useState(false)
    const [activeBank, setActiveBank] = useState(parentProp && parentProp.location && parentProp.location.state && parentProp.location.state["bank"] && parentProp.location.state.bank["id"] ? parentProp.location.state.bank : { id: null })
    const [isCompleteLoading, setIsCompleteLoding] = useState(false)
    const [showpop, setShowPop] = useState(false)
    const [toogleNew, setToggleNew] = useState(false)
    const [reload, setReload] = useState(false)
    const [newAccount, setNewAccount] = useState({})


    const loadAccount = () => {
        setAccountLoad(true)
        LModel.find('accounts', null, activeBank && activeBank["id"] ? `bank=${activeBank["id"]}` : null)
            .then(res => {
                // if (res && res.results) {
                setAccountLoad(false)
                setActiveAccount(res[0])
                setAccounts(res)
                // setActiveIdx(0)
                // setCTransaction(res.results)
                // }
            }).catch(e => {
                setAccountLoad(false)
                console.log(e)
            })
    }

    const loadBank = () => {
        setBankLoad(true)
        LModel.find('bank')
            .then(res => {
                setBankLoad(false)
                if (res) {
                    setBank(res)
                    if (!(activeBank && activeBank.id)) {
                        setActiveBank(res[0])
                    }
                }
            }).catch(e => {
                setBankLoad(false)
                console.log(e)
            })
    }





    const deleteAccount = () => {
        if (activeAccount && activeAccount.id) {
            setIsCompleteLoding(true)
            LModel.destroy(`accounts`, activeAccount.id)
                .then(res => {
                    setIsCompleteLoding(false)
                    // if (res && res.id) {
                    setShowPop(false)
                    setReload(!reload)
                    // }
                }).catch(e => {
                    setIsCompleteLoding(false)
                    console.log(e)
                })
        }
    }


    // useEffect(() => {
    //     setActiveBank(activeBank)
    // }, [reloadParentPage])


    useEffect(() => {
        loadBank()
    }, [reload])

    useEffect(() => {
        loadAccount()
    }, [activeBank])

    useEffect(() => {
        setParentActiveCurrency(activeAccount)
    }, [activeAccount])




    useEffect(() => {
        if (newAccount && newAccount.id) {
            setToggleNew(true)
        }
    }, [newAccount])



    // useEffect(() => {
    //     if (!toogleNew) {
    //         setNewAccount()
    //     }
    //     console.log("toggle " + toogleNew)
    // }, [toogleNew])




    const PopupChild = () => {

        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want to delete
                    {activeAccount && activeAccount["holder"] ?
                            <span className="mx-1 text-blue-700 font-semibold">{activeAccount["holder"]}'s</span> : '--'}
                    bank account and its transaction</p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => deleteAccount()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{isCompleteLoading ? <CircularLoading loading={isCompleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )

    }

    const BankMgmnt = () => {

        const [show, setShow] = useState(false)

        const filterMenu = useRef(null);
        DropDownToggler(filterMenu);

        function DropDownToggler(ref) {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    setShow(false)
                } else {
                    setShow(true)
                }
            }

            useEffect(() => {
                // Bind the event listener
                document.addEventListener("mousedown", handleClickOutside, true);
                return () => {
                    // Unbind the event listener on clean up
                    document.removeEventListener("mousedown", handleClickOutside, true);
                };
            });
        }




        return (
            <div className="relative text-center">
                <div className="relative inline-flex p-1 my-auto">
                    {
                        bankLoad ?
                            <CircularLoading loading={bankLoad} color={"bg-gray-500"} />
                            :
                            <button
                                onClick={() => setShow(!show)}
                                className="px-3 inline rounded-l bg-white border p-1 text-gray-700 hover:bg-blue-400 hover:text-white"> {activeBank ? activeBank["name"] : '--'}
                                <ChevronDown size={15} className="inline mx-1" />
                            </button>
                    }
                    <button onClick={() => {
                        setNewAccount()
                        setToggleNew(!toogleNew)
                    }} className="px-2 rounded-r bg-white border p-1 text-gray-700 hover:bg-green-600 hover:text-white">
                        <Plus size={15} strokeWidth="4" />
                    </button>
                </div>


                { bank && bank.length > 0 ?
                    <ul ref={filterMenu} className={`bg-gray-100 border-2 rounded absolute h-32 min-h-0 w-24 overflow-y-auto left-0 z-10 ${show ? "" : "hidden"}`}  >
                        {
                            bank.map((bnk, indx) => {
                                return (
                                    <li key={indx} className="m-1">
                                        <div onClick={() => setActiveBank(bnk)}
                                            className={`flex px-2 py-1 hover:bg-blue-200 ${bnk["id"] === activeBank["id"] ? 'bg-blue-200 border border-white' : null}`}>
                                            <p className="px-2 text-sm text-gray-700">{bnk["name"]}</p>
                                        </div>
                                    </li>
                                )

                            })
                        }
                    </ul>
                    :
                    null
                }
            </div>
        )
    }




    return (
        <Fragment>
            {
                accountLoad ?
                    <div className="flex justify-center">
                        <CurrencyLoading />
                    </div >
                    :
                    <div className="bg-gray-400 mr-3 w-2/12 rounded-sm border p-1 overflow-y-auto" style={{ height: "calc(95vh - 100px)" }}>

                        <BankMgmnt />

                        {toogleNew ?
                            <NewAccount
                                toogleNAcc={() => {
                                    setToggleNew(false)
                                    setNewAccount()
                                }}
                                reloadPage={() => setReload(!reload)}
                                activeBank={activeBank}
                                activeAcc={newAccount}
                            />
                            :
                            null
                        }


                        {
                            accounts && accounts.length > 0 ?

                                accounts.map((crncy, index) => (
                                    <AccountCard
                                        key={index}
                                        crncy={crncy}
                                        activeAccount={activeAccount}
                                        setActiveAccount={(dt) => setActiveAccount(dt)}
                                        setShowPop={() => setShowPop(true)}
                                        setToggleNew={(e) => setToggleNew(e)}
                                        setNewAccount={(e) => setNewAccount(e)}
                                    />

                                ))
                                :
                                <div className="text-center p-4">
                                    <div className="">
                                        {/* <p className={searchKey === undefined ? 'hidden' : 'block text-gray-800 text-2xl font-thin'}> {`Not Found ${searchKey}`}</p> */}
                                        <p>
                                            <FolderPlus onClick={() => setToggleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-white" size={100} color={'gray'} strokeWidth={0.5} />
                                        </p>
                                        {/* <em>Account list is Empty.</em> */}
                                    </div>
                                </div>
                        }
                    </div>
            }

            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<PopupChild />}
                parentCss={'w-5/12 bg-white rounded p-6'}

            />
        </Fragment>

    )
}

export default BankSelection;