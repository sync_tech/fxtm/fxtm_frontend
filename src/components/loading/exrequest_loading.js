import React from 'react'
import ContentLoader from 'react-content-loader'

const ExRequestLoader = props => {
  // Get values from props
  // const { rows, columns, coverHeight, coverWidth, padding, speed } = props;

  // Hardcoded values
  const rows = 5
  const columns = 4
  const coverWidth = 200
  const coverHeight = 175
  const padding = 40
  const speed = 0.75

  const coverHeightWithPadding = coverHeight + padding
  const coverWidthWithPadding = coverWidth + padding
  const initial = 35
  const covers = Array(columns * rows).fill(1)

  return (
    <ContentLoader
      speed={speed}
      width={(columns * coverWidthWithPadding) - padding}
      height={rows * coverHeightWithPadding}
      backgroundColor={'#FFF'}
    //   primaryColor="#242b34"
    //   secondaryColor="#343d4c"
      {...props}
    >

      {covers.map((g, i) => {
        let vy = Math.floor(i / columns) * coverHeightWithPadding + initial
        let vx = (i * coverWidthWithPadding) % (columns * coverWidthWithPadding)
        return (
          <rect
            key={i}
            x={vx}
            y={vy}
            ry={6}
            rx={6}
            width={coverWidth}
            height={coverHeight}
          />
        )
      })}
    </ContentLoader>
  )
}

ExRequestLoader.metadata = {
  name: 'Samson A',
}

export default ExRequestLoader