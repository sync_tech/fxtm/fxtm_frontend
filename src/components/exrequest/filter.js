import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'

export default ({ FilterMenu, setFilter, filter }) => {

    const filterSetter = (newF) => {
        let prev_filter = filter
        let new_filter = ""

        if (newF) {

            if (prev_filter && prev_filter.includes(newF)) {
                new_filter = prev_filter.replace(newF, '')
                setFilter(new_filter)

            } else {
                new_filter = prev_filter + newF
                setFilter(new_filter)
            }

        } else {
            setFilter("")
        }
    }

    return (
        FilterMenu && FilterMenu.length > 0 ?
            <ul className="my-auto">
                {
                    FilterMenu.map(fm => {
                        return (
                            <li onClick={() => filterSetter(fm.value)} key={fm.label} className={`inline-block px-4 rounded border text-xs p-1 ${(filter === "" && fm.label === "All") || filter && filter.includes(fm.value) ? 'text-blue-700 bg-white' : 'text-white bg-blue-700 hover:bg-blue-500'}`}><Link to="#"> {fm.label} </Link></li>
                        )
                    })

                }
            </ul>
            :
            null
    )
}