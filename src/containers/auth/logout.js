import React, { useEffect } from 'react'
import { useAuth } from '../../providers/auth_provider'
import { useHistory } from '../../providers/history_provider';
import Loading from '../../components/loading/dot_loading'

const LogOut =  props => {

    const {authState, logout} = useAuth()
    const {history} = useHistory()
    // logout user
    useEffect(() => {
        logout()
    }, [])

    // redirect after logout
    useEffect(() => {
        setTimeout(() => {
            history.push('login')
        }, 3000);
    }, [])

    return (
        <div className="pageloader"><span className="title"> <Loading loading={true} txt={"Loging out"} color={"bg-gray-600"}/> </span></div>
    )
    
}

export default LogOut;