import React from 'react';
import Menu from './menu';

export default ({path,toggleSideBar, setToggleSideBar}) => {
    
    return(
        <div className={`bg-sideNav fixed ${ toggleSideBar ? 'w-20' : 'w-56'} h-full no-print`}>
            {/* md:w-20 lg:w-56 */}
            <div className="h-full" style={{height:"calc(100% - 90px)"}}>
                <h1 onClick={() => setToggleSideBar(!toggleSideBar)} className="text-xl text-gray-300 text-center font-semibold p-4 mt-2 hover:text-white">{ toggleSideBar ? 'FXTM' : 'FXTM DASHBOARD'}</h1>
                
                <Menu toggleSideBar={toggleSideBar} setToggleSideBar={(v)=>setToggleSideBar(v)} path={path}/> 
            </div>
        </div>
    )
}