import { LOGIN_REQUEST, LOGIN_REQUEST_SUCCESS, LOGIN_REQUEST_ERROR, LOGOUT_REQUEST, SIGNUP_REQUEST, SIGNUP_REQUEST_SUCCESS, SIGNUP_REQUEST_ERROR, RESETPASSWORD_REQUEST, RESETPASSWORD_REQUEST_SUCCESS, RESETPASSWORD_REQUEST_ERROR, LOGOUT_REQUEST_SUCCESS, CHECK_IS_LOGGEDIN, CHECK_IS_LOGGEDIN_SUCCESS, CHECK_IS_LOGGEDIN_ERROR, CHECK_IS_ROLE_SET, CHECK_IS_ROLE_SET_SUCCESS, CHECK_IS_ROLE_SET_ERROR } from "../constants/action_constants";
import LModel from "../services/api";
import AuthService from "../services/auth_service";

export const login = (dispatch) => (data, remember) => {

    dispatch({
        type: LOGIN_REQUEST,
        payload: {
            data: data,
            loading: true
        }
    })

    LModel.create('token/', data)
        .then(response => {

            AuthService.save_token(response, remember)

            setTimeout(() => {
                dispatch({
                    type: LOGIN_REQUEST_SUCCESS,
                    payload: {
                        loading: false,
                        data: response
                    }
                })
            }, 2000)
            // save data
        })
        .catch(error => {
            dispatch({
                type: LOGIN_REQUEST_ERROR,
                payload: {
                    error: error,
                    loading: false
                }
            })
        })
}

export const signup = (dispatch) => (data) => {

    dispatch({
        type: SIGNUP_REQUEST,
        payload: {
            data: data,
            loading: true
        }
    })

    LModel.create('Accounts', data)
        .then(response => {
            AuthService.save_token(response, true)

            setTimeout(() => {
                dispatch({
                    type: SIGNUP_REQUEST_SUCCESS,
                    payload: {
                        loading: false,
                        data: response
                    }
                })
            }, 2000)
            // save data
        })
        .catch(error => {
            dispatch({
                type: SIGNUP_REQUEST_ERROR,
                payload: {
                    error: error,
                    loading: false
                }
            })
        })
}

export const logout = (dispatch) => () => {
    dispatch({
        type: LOGOUT_REQUEST
    })

    AuthService.remove_token()

    // LModel.create('Accounts/logout', {})
    //     .then(response => {
    dispatch({
        type: LOGOUT_REQUEST_SUCCESS
    })
    //         AuthService.remove_token()
    //     })
    //     .catch(error => {
    //         dispatch({
    //             type: LOGIN_REQUEST_ERROR,
    //             payload: {
    //                 error: error
    //             }
    //         })
    //         AuthService.remove_token()
    //     })
}

export const reset_password = (dispatch) => (data) => {

    dispatch({
        type: RESETPASSWORD_REQUEST,
        payload: {
            data: data,
            loading: true
        }
    })

    LModel.create('Accounts/reset-password', data)
        .then(response => {
            //AuthService.save_token(response, true)

            setTimeout(() => {
                dispatch({
                    type: RESETPASSWORD_REQUEST_SUCCESS,
                    payload: {
                        loading: false,
                        data: response
                    }
                })
            }, 2000)
            // save data
        })
        .catch(error => {
            dispatch({
                type: RESETPASSWORD_REQUEST_ERROR,
                payload: {
                    error: error,
                    loading: false
                }
            })
        })
}

export const getRole = (dispatch) => () => {

    dispatch({
        type: CHECK_IS_ROLE_SET,
        payload: {
            loading: true
        }
    })

    LModel.find('user/')
        .then(res => {
            let grp = res.map(g => g && g.name ? g.name : null)

            AuthService.save_group(grp)

            // setTimeout(() => {
                dispatch({
                    type: CHECK_IS_ROLE_SET_SUCCESS,
                    payload: {
                        loading: false,
                        roleSetSuccess: true,
                        roles : grp
                    }
                })
            // }, 2000)

        }).catch(error => {

            dispatch({
                type: CHECK_IS_ROLE_SET_ERROR,
                payload: {
                    error: error,
                    loading: false,
                    roleSetSuccess: false
                }
            })

        })

}
