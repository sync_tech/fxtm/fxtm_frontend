import React, { useState, useEffect, Fragment } from 'react';
import AutoCompleteSearch from '../../components/exrequest/client_search';
import moment from 'moment';
import { FolderPlus, Plus } from 'react-feather';
import LModel from '../../services/api';
import Filter from '../../components/ui/filter';
import GridLoading from '../../components/loading/exrequest_loading';
import NewMemo from './new_memo';
import MemoCard from './memo_card';
import Pagination from '../../components/ui/pagination';

import CircularLoading from '../../components/loading/circular_loading';
import Popup from '../../components/ui/popup';

const MemoList = () => {


    const [loading, setLoading] = useState(false)
    const [memo, setMemo] = useState([])
    const [memoInfo, setMemoInfo] = useState({ previous: null, next: null, count: 0 })


    const [filter, setFilter] = useState(null)

    const [reload, setReload] = useState(false)
    const [toogleNew, setToogleNew] = useState(false)

    const [searchKey, setSearchKey] = useState("")
    const [circularloading, setCircularloading] = useState(false)
    const [paginationIndex, setPaginationIndex] = useState(1)

    const [editMemo, setEditMemo] = useState()
    const [showpop, setShowPop] = useState(false)
    const [deleteLoading, setDeleteLoading] = useState(false)


    const loadMemo = () => {
        setLoading(true)
        LModel.find('memo', null, filter !== null ? `${filter}&name=${searchKey}&page=${1}` : `&name=${searchKey}&page=${paginationIndex}`)
            .then(res => {
                if (res && res.results) {
                    setLoading(false)
                    setMemo(res.results)
                    setMemoInfo(res)
                }
            }).catch(e => {
                setLoading(false)
                console.log(e)
            })
    }






    useEffect(() => {
        loadMemo()
    }, [searchKey, reload, filter, paginationIndex])


    useEffect(() => {
        if (editMemo && editMemo.id) {
            setToogleNew(true)
        }
        if (showpop) {
            setToogleNew(false)
        }
    }, [editMemo])

    // useEffect(() => {
    //     if (toogleNew) {
    //         setEditMemo()
    //     }
    // }, [toogleNew])


    const deleteMemo = () => {
        if (editMemo && editMemo.id) {
            setDeleteLoading(true)
            LModel.destroy('memo', editMemo.id)
                .then(res => {
                    // if (res && res.results)  {
                    setDeleteLoading(false)
                    setShowPop(false)
                    loadMemo()
                    // }
                }).catch(e => {
                    setDeleteLoading(false)
                    console.log(e)
                })
        }
    }

    const MemoDeletePopUp = () => {

        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want delete
                    <span className="text-blue-800 font-semibold italic mx-1">{editMemo && editMemo.title? editMemo.title : '--'}</span>
                    </p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => deleteMemo()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )
    }



    return (
        <div className="mx-4 p-4">
            <div className="flex justify-between">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadMemo}
                />

                <div className="inline-flex">

                    {!toogleNew ?
                        <div className="flex justify-end mx-3">
                            <button onClick={() => {
                                setEditMemo()
                                setToogleNew(true)
                            }
                            } className="border rounded-md float-riht border-gray-600 p-2 my-1 hover:bg-green-700 hover:stroke-white stroke-green px-3">
                                <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                                    ADD NEW
                                </button>
                        </div> :
                        null
                    }

                    <Filter filter={filter} setFilter={setFilter} />
                </div>
            </div>



            {
                loading ?
                    <div className="flex justify-center">
                        <GridLoading />
                    </div>
                    :



                    <Fragment>

                        {!toogleNew ?

                            null
                            :
                            <NewMemo closeSave={() => setToogleNew(false)} reloadPage={() => setReload(!reload)} editMemo={editMemo} />
                        }

                        {memo && !loading && memo.length > 0 ?
                            <div className="grid grid-cols-4 gap-4 mt-6 m-10">
                                {
                                    memo.map((mem, idx) => {
                                        return <MemoCard key={idx} res={mem} setToEdit={(m) => setEditMemo(m)} closeNew={() => setToogleNew(false)} setShowPop={() => setShowPop(true)}
                                        />
                                    })
                                }
                            </div>
                            :
                            <div className="w-full text-center p-4">

                                <div className="">
                                    <p className={searchKey === undefined ? 'hidden' : 'block text-gray-800 text-2xl font-thin'}> {`Not Found ${searchKey}`}</p>
                                    <p>
                                        <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-white" size={140} color={'gray'} strokeWidth={0.5} />
                                    </p>
                                </div>

                            </div>
                        }

                    </Fragment>
            }

            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<MemoDeletePopUp />}
                parentCss={'w-5/12 bg-white rounded p-6'}
            />
            
            <Pagination
                count={memoInfo.count}
                previous={memoInfo.previous}
                next={memoInfo.next}
                list={20}
                activeIndex={paginationIndex}
                setIndex={(i) => setPaginationIndex(i)}
            />

        </div>

    )

}

export default MemoList;