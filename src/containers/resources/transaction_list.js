import React, { useState, useEffect, Fragment } from 'react';
import { Folder, ChevronDown } from 'react-feather';
import LModel from '../../services/api';
import moment from 'moment';
import ResourceLoading from '../../components/loading/resource_loading';
import Pagination from '../../components/ui/pagination'
import { MoneyFormat } from '../../services/money_format';
import TableLoading from '../../components/loading/table_loading';

// default select one
// on click list all that currency transactions(credits)
//


const TransactionList = ({ filter, loading, ctransaction, ctransactionInfo, searchKey, paginationIndex, setPaginationIndex }) => {


    return (

        <Fragment>

            {
                loading ?
                    <div className="flex justify-center">
                        <ResourceLoading />
                    </div>
                    :
                    ctransaction && ctransaction.length > 0 ?
                        <table className="w-full mt-4 bg-white">
                            <thead className="bg-blue-700">
                                <tr className="border p-2 text-white">
                                    <th className="border-r border-gray-400 p-2 text-center">#</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Client</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Amount</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Rate</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">currency</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Birr</th>
                                    <th className="border-r border-gray-400 font-semibold text-center">Transactor<ChevronDown className="inline float-right hover:bg-blue-800 m-1" size={18} /></th>
                                    <th className="border-r border-gray-400 font-semibold text-center">On</th>
                                    {/* <th className="border-r border-gray-400 pr-2"><FontAwesomeIcon icon={faPlusCircle} onClick={() => history.push('/dashboard/vehicles/new')} title="Add New Vehicle" className="text-4xl float-right hover:text-blue-800 text-blue-500 hover:shadow-outline border rounded-full h-7 w-7" /></th> */}
                                </tr>
                            </thead>
                            <tbody className="">
                                {
                                    ctransaction.map((tran, index) => (
                                        <tr key={index} className={`border cursor-pointer hover:bg-gray-200 text-center ${tran && tran.exRequest && tran.exRequest.is_completed === true? 'bg-green-100 hover:bg-green-100' : ''}`}>
                                            <td className="border-r p-2 text-sm">{filter && filter.initialDate ? index + 1 : index + 1 + ((paginationIndex - 1) * 20)}</td>

                                            <td className="border-r text-gray-900 text-sm">{tran && tran.exRequest && tran.exRequest.client && tran.exRequest.client.name ? tran.exRequest.client.name : '--'}</td>
                                            <td className="border-r text-gray-900 text-sm">{MoneyFormat(tran.requested_amount)}</td>
                                            <td className="border-r text-gray-900 text-sm">{tran.rate}</td>
                                            <td className="border-r text-gray-900 text-sm">{tran.currency && tran.currency.code ?
                                                <Fragment>
                                                    <img src={tran.currency.flag} className="w-5 h-4 inline mx-1" />  {tran.currency.code}
                                                </Fragment>
                                                :
                                                '--'}
                                            </td>
                                            <td className="border-r text-gray-900 font-semibold text-sm">{MoneyFormat(tran.birr)}</td>
                                            <td className="border-r text-gray-900 text-sm">{'Jon'}</td>
                                            <td className="border-r p-2 flex justify-evenly">
                                                {/* <Link to={`vehicles/detail/${v.id}`} className={`active:outline-none p-2 bg-transparent rounded hover:text-white hover:bg-blue-500 text-blue-700 px-4 mr-2`} title={`Click to view detail`}>More...</Link> &nbsp; */}
                                                {/* <Link to={`vehicles/edit/${v.id}`} className={`active:outline-none p-2 bg-transparent rounded hover:text-white focus:shadow-outline hover:bg-blue-500 text-blue-700 px-4`} title={`Click to edit`}>Edit</Link> */}
                                                <p className="my-auto text-sm text-gray-900">{moment(tran.created_at).format('ll')}</p>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                        :
                        <div className="w-full text-center p-10">

                            <div className="">
                                <p>
                                    <Folder className="inline-flex text-center rounded-lg m-2" size={140} color={'gray'} strokeWidth={0.5} />
                                </p>
                                <em>Transaction  <span className="italic text-lg">{searchKey}</span> is not found.</em>
                            </div>
                        </div>
            }


            <Pagination
                count={ctransactionInfo.count}
                previous={ctransactionInfo.previous}
                next={ctransactionInfo.next}
                list={20}
                activeIndex={paginationIndex}
                setIndex={(i) => setPaginationIndex(i)}
            />
        </Fragment>


    )
}

export default TransactionList;


// Questions: Transaction list should separate or merge togther ??