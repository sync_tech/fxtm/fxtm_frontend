import React from 'react';
import { Redirect } from 'react-router-dom';


// home routes
import Dashboard from './containers/dashboard/index';
import Home from './containers/home/index';
import Client from './containers/client/index';
import ExRequest from './containers/exrequest/index';
import Loans from './containers/loans/index';
import Transaction from './containers/transaction/index';
import Account from './containers/accounts/index';
import Resources from './containers/resources/index';
import Report from './containers/report/index';
import Admin from './containers/admin/index';

// error route
import NotFound from './containers/error/notfound';

// authentication routes
import Auth from './containers/auth/index';
import Login from './containers/auth/login';
import Logout from './containers/auth/logout';
import Signup from './containers/auth/signup';
import ForgetPassword from './containers/auth/forget_password';
import ResetPassword from './containers/auth/reset_password'

/*
list client
new client
detail
edit
flag as delete
*/

//Client routes
import ListClient from './containers/client/list_client';
import NewClient from './containers/client/new_client';
import EditClient from './containers/client/edit_client';
// import DetailClient from './containers/client/detail_client';

//Exchange Routes
import ListExRequest from './containers/exrequest/list_exrequest';
import ExRequestTransaction from './containers/exrequest/exRequest_transaction';

// Transaction Routs
import DebitTransaction from './containers/transaction/debit_transaction';
import CreditTransaction from './containers/transaction/credit_transaction';

//Loan Transaction
import LoanList from './containers/loans/list_loan';
import LoanTransaction from './containers/loans/loan_transaction';

// Memo
import MemoList from './containers/memo/index';


// Admin
import UserList from './containers/admin/list_users';

// Bank account
import BankList from './containers/accounts/list_bank';
import BankTransactions from './containers/accounts/bank_transaction';

// TODO:
// 0) auth_route
// 1) user_and_client_route
// 2) exchange_request_route
// 3) resource_route
// 4) transaction_route
// 5) report_route


export const routes = [
    {
        path: '/dashboard',
        title: '',
        name: 'Dashboard',
        // exact: true,
        private: true,
        // private: false,
        component: Dashboard,
    },
    {
        path: '/auth',
        title: 'auth',
        name: '',
        // exact: true,
        private: false,
        component: Auth,

    },
    {
        path: '/',
        title: 'Dashboard',
        name: '',
        exact: true,
        private: true,
        // private: false,
        component: Dashboard,

    },
    {
        path: '/:anything',
        title: '404- Not Found',
        name: 'Not Found',
        component: NotFound
    }
]

export const authRoutes = [
    {
        path: '/auth/',
        title: 'Login',
        exact: true,
        name: 'Login',
        component: Login,
    },
    {
        path: '/auth/login',
        title: 'Login',
        exact: true,
        name: 'Login',
        component: Login,
    },
    {
        path: '/auth/logout',
        title: 'Logout',
        exact: true,
        name: 'Logout',
        component: Logout
    },
    {
        path: '/auth/signup',
        title: 'SignUp',
        exact: true,
        name: 'SignUp',
        component: Signup
    },
    {
        path: '/auth/reset-password',
        title: 'Reset Password',
        exact: true,
        name: 'Reset Password',
        component: ForgetPassword,
    },
    {
        path: '/auth/forget_password',
        title: 'Forget Password',
        exact: true,
        name: 'Forget Password',
        component: ResetPassword
    },
]

export const dashboardRoutes = [
    {
        path: '/',
        title: 'Home',
        exact: true,
        name: '',
        component: Home,
    },
    {
        path: '/dashboard',
        title: 'Home',
        exact: true,
        name: '',
        component: Home,
    },
    {
        path: '/dashboard/home',
        title: 'Home',
        exact: true,
        name: '',
        component: Home,
    },
    {
        path: '/dashboard/client',
        title: 'Client',
        // exact: true,
        name: '',
        component: Client,
    },
    {
        path: '/dashboard/transaction',
        title: 'Transaction',
        // exact: true,
        name: '',
        component: Transaction,
    },
    {
        path: '/dashboard/exrequest',
        title: 'ExRequest',
        // exact: true,
        name: '',
        component: ExRequest,
    },
    {
        path: '/dashboard/loan',
        title: 'ExRequest',
        // exact: true,
        name: '',
        component: Loans,
    },
    {
        path: '/dashboard/memo',
        title: 'Memo',
        // exact: true,
        name: '',
        component: MemoList,
    },
    {
        path: '/dashboard/resources',
        title: 'Resources',
        exact: true,
        name: '',
        component: Resources,
    },
    {
        path: '/dashboard/account',
        title: 'Account',
        // exact: true,
        name: '',
        component: Account,
    },
    {
        path: '/dashboard/report',
        title: 'ExRequest',
        exact: true,
        name: '',
        component: Report,
    },
    {
        path: '/dashboard/admin',
        title: 'ExRequest',
        // exact: true,
        name: '',
        component: Admin,
    },
]

export const clientRoutes = [
    {
        path: '/dashboard/client/',
        title: 'Client',
        exact: true,
        name: '',
        component: ListClient,
    },
    {
        path: '/dashboard/client/list',
        title: 'Client',
        exact: true,
        name: '',
        component: ListClient,
    },
    {
        path: '/dashboard/client/new',
        title: 'Client',
        exact: true,
        name: '',
        component: NewClient,
    },
    {
        path: '/dashboard/client/edit/:id',
        title: 'Client',
        exact: true,
        name: '',
        component: EditClient,
    },
    // {
    //     path: '/dashboard/client/detail/:id',
    //     title: 'Client',
    //     exact: true,
    //     name: '',
    //     component: DetailClient,
    // },
    // {
    //     path: '/:anything',
    //     title: '404- Not Found',
    //     name: 'Not Found',
    //     component: NotFound
    // }

]

export const exRequestRoutes = [
    {
        path: '/dashboard/exrequest',
        title: 'ExRequest',
        exact: true,
        name: '',
        component: ListExRequest,
    },
    {
        path: '/dashboard/exrequest/list',
        title: 'ExRequest',
        exact: true,
        name: '',
        component: ListExRequest,
    },
    {
        path: '/dashboard/exrequest/:id',
        title: 'ExRequest',
        exact: true,
        name: '',
        component: ExRequestTransaction,
    },
    // {
    //     path: '/dashboard/client/detail/:id',
    //     title: 'Client',
    //     exact: true,
    //     name: '',
    //     component: DetailClient,
    // },
]

export const loanRoutes = [
    {
        path: '/dashboard/loan',
        title: 'Loan',
        exact: true,
        name: '',
        component: LoanList,
    },
    // {
    //     path: '/dashboard/loan/list',
    //     title: 'Loan',
    //     exact: true,
    //     name: '',
    //     component: LoanList,
    // },
    {
        path: '/dashboard/loan/:id',
        title: 'Loan',
        exact: true,
        name: '',
        component: LoanTransaction,
    },
]

export const transactionRoutes = [
    {
        path: '/dashboard/transaction/debit',
        title: 'Debit Transaction',
        exact: true,
        name: '',
        component: DebitTransaction,
    },
    {
        path: '/dashboard/transaction/credit',
        title: 'Credit Transaction',
        exact: true,
        name: '',
        component: CreditTransaction,
    },
]

export const bankRoutes = [
    {
        path: '/dashboard/account',
        title: 'Bank Account',
        exact: true,
        name: '',
        component: BankList,
    },
    {
        path: '/dashboard/account/list',
        title: 'Bank Account',
        exact: true,
        name: '',
        component: BankList,
    },
    {
        path: '/dashboard/account/:id',
        title: 'Bank Account',
        exact: true,
        name: '',
        component: BankTransactions,
    }
]

export const adminRoutes = [
    {
        path: '/dashboard/admin',
        title: 'Admin',
        exact: true,
        name: '',
        component: UserList,
    },
    // {
    //     path: '/dashboard/loan/list',
    //     title: 'Loan',
    //     exact: true,
    //     name: '',
    //     component: LoanList,
    // },
    {
        path: '/dashboard/admin/:id',
        title: 'Admin',
        exact: true,
        name: '',
        component: UserList,
    },
]


// export const accountRoutes = [
//     {
//         path: '/dashboard/account',
//         title: 'Loan',
//         exact: true,
//         name: '',
//         component: LoanList,
//     },
//     // {
//     //     path: '/dashboard/loan/list',
//     //     title: 'Loan',
//     //     exact: true,
//     //     name: '',
//     //     component: LoanList,
//     // },
//     {
//         path: '/dashboard/loan/:id',
//         title: 'Loan',
//         exact: true,
//         name: '',
//         component: LoanTransaction,
//     },
// ]

// export const homeRoutes = [
//     {
//         path: '/',
//         title: 'HOF-FMS - ',
//         name: 'Home',
//         exact: true,
//         component: () => <Redirect to='/dashboard' />,
//     },
//     {
//         path: '/:anything',
//         title: '404- Not Found',
//         name: 'Not Found',
//         component: NotFound
//     }
// ]




// export const authRoutes = [
//     {
//         path: '/auth/login',
//         title: 'HOF-FMS - Login',
//         exact: true,
//         name: 'Login',
//         component: Login,
//     },
//     {
//         path: '/auth/logout',
//         exact: true,
//         name: 'Logout',
//         title: 'HOF-FMS - Logout',
//         component: Logout
//     },
//     {
//         path: '/auth/reset-password',
//         exact: true,
//         private: true,
//         name: 'Reset Password',
//         component: ForgetPassword,
//     }
// ]

// export const dashboardRoutes = [
//     {
//         path: '/dashboard',
//         exact: true,
//         name: 'Dashboard',
//         // private: true,
//         private: true,
//         title: 'HOF-FMS - Dashboard',
//         component: DashboardHome,
//     },
//     {
//         path: '/dashboard/vehicles',
//         // exact: true,
//         name: 'Vehicles',
//         private: true,
//         title: 'Vehicles',
//         component: vehicles
//     },
//     {
//         path: '/dashboard/trips',
//         // exact: true,
//         name: 'Trips',
//         private: true,
//         title: 'HOF-FMS - Trips',
//         component: Trip
//     },
//     {
//         path: '/dashboard/fuel',
//         exact: true,
//         name: 'Refilling Request',
//         title: 'HOF-FMS - Refilling Request',
//         private: true,
//         component: Fuel,
//     },
//     {
//         path: '/dashboard/incidents',
//         exact: true,
//         name: 'Incidents',
//         private: true,
//         title: 'HOF-FMS - Incidents',
//         component: Incidents
//     },
//     {
//         path: '/dashboard/incidents/new',
//         exact: true,
//         name: 'New-Incidents',
//         private: true,
//         title: 'HOF-FMS - New Incidents',
//         component: NewIncident,
//         type: 'create'
//     },
//     {
//         path: '/dashboard/incidents/detail/:id',
//         exact: true,
//         name: 'Incident-Detail',
//         private: true,
//         title: 'HOF-FMS - Incident Detail',
//         component: IncidentDetail,
//     },
//     {
//         path: '/dashboard/incidents/edit/:id',
//         exact: true,
//         name: 'Edit-Incident',
//         private: true,
//         title: 'HOF-FMS - Edit Incident',
//         component: EditIncident,
//     },
//     {
//         path: '/dashboard/incidents/report/:id',
//         exact: true,
//         name: 'Edit-Incident',
//         private: true,
//         title: 'HOF-FMS - Edit Incident',
//         component: IncidentReport,
//     },
//     {
//         path: '/dashboard/settings',
//         // exact: true,
//         name: 'Setting',
//         private: true,
//         title: 'HOF-FMS - Setting',
//         component: Setting
//     },{
//         path: '/dashboard/services',
//         // exact: true,
//         name: 'Service',
//         private: true,
//         title: 'Service',
//         component: Service
//     },
//     {
//         path: '/dashboard/reports',
//         // exact: true,
//         name: 'Report',
//         private: true,
//         title: 'Report',
//         component: Reports
//     },
//     {
//         path: '/dashboard/users',
//         exact: true,
//         name: 'Users',
//         private: true,
//         component: UsersList
//     },
//     {
//         path: '/dashboard/departments',
//         exact: true,
//         name: 'Departments',
//         private: true,
//         component: Departments,
//     }
// ]


// export const routes = [
//     {
//         path: '/',
//         title: 'Landing',
//         name: 'landing',
//         exact: true,
//         component: Landing,
//         // redirect: 'Home',
//         private : false
//     },
//     {
//         path: '/auth',
//         title: 'Login',
//         name: 'Login',
//         // exact: true,
//         component: Auth,
//         private: false,
//     },
//     {
//         path: '/home',
//         title: 'Seleda',
//         name: 'Home',
//         // exact: true,
//         component: Home,
//         private: true,
//     },
//     {
//         path: '/user',
//         title: 'User',
//         // exact: true,
//         name: 'User Profile',
//         private: true,
//         component: User,
//     },
//     {
//         path: '/:anything',
//         title: '404- Not Found',
//         name: 'Not Found',
//         component: NotFound
//     }
// ]