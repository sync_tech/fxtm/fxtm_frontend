import React, { useState, useEffect, Fragment } from 'react';
import { Folder, FolderPlus, Plus, Edit3, Trash2 } from 'react-feather';
import LModel from '../../services/api';
import moment from 'moment';
import ResourceLoading from '../../components/loading/resource_loading';
import Pagination from '../../components/ui/pagination'
import { MoneyFormat } from '../../services/money_format';
import TableLoading from '../../components/loading/table_loading';
import NewTransaction from './transaction_new';
import Popup from '../../components/ui/popup';
import CircularLoading from '../../components/loading/circular_loading';


// const TransactionList = ({ filter, loading, ctransaction, ctransactionInfo, searchKey, paginationIndex, setPaginationIndex }) => {
const TransactionList = ({ activeAccount,reloadParentPage, setActiveAccount }) => {
    const [loading, setLoading] = useState(false)
    const [transaction, setTransaction] = useState([])
    const [tinfo, setTinfo] = useState([])
    const [newtran, setNewTran] = useState(false)
    const [IsDepo, setIsDepo] = useState(false)
    const [reload, setReload] = useState(false)
    const [showpop, setShowPop] = useState(false)
    const [deleteLoading, setDeleteLoading] = useState(false)
    const [activeTransaction, setActiveTransaction] = useState()


    const loadTransactions = () => {
        if (activeAccount && activeAccount['id']) {

            setLoading(true)
            LModel.find('bank_transaction', null, `account=${activeAccount['id']}`)
                .then(res => {
                    setLoading(false)
                    setTransaction(res)
                    // if (res && res.results) {
                    //     // set(res)
                    // }
                }).catch(e => {
                    setLoading(false)
                    console.log(e)
                })
        }
    }


    




    useEffect(() => {
        if (activeAccount && activeAccount.id) {
            reloadParentPage()
            loadTransactions()
        }
    }, [activeAccount, reload])



    useEffect(() => {
        if (activeTransaction && activeTransaction.id) {
            setNewTran(true)
        }
        if (showpop) {
            setNewTran(false)
        }
    }, [activeTransaction])



    const deleteAccount = () => {
        if (activeTransaction && activeTransaction.id) {
            setDeleteLoading(true)
            LModel.destroy(`bank_transaction`, activeTransaction.id)
                .then(res => {
                    setDeleteLoading(false)
                    // if (res && res.id) {
                    setShowPop(false)
                    setReload(!reload)
                    // }
                }).catch(e => {
                    setDeleteLoading(false)
                    console.log(e)
                })
        }
    }


    const PopupChild = () => {

        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want to delete the transaction of
                    {activeAccount && (activeTransaction["deposit"] || activeTransaction["withdraw"]) ?
                            <span className="mx-1 text-blue-700 font-semibold">{activeTransaction["deposit"] || activeTransaction["withdraw"]} birr amount </span> : '--'}
                    </p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => deleteAccount()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">
                        {deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}
                    </button>
                </div>
            </div>
        )

    }



    return (
        <Fragment>
            { loading ?
                <ResourceLoading />
                :
                <Fragment>
                    {
                        newtran ?
                            <NewTransaction
                                toogleNTran={(e) => setNewTran(e)}
                                account={activeAccount}
                                reloadPage={() => setReload(!reload)}
                                nwTransaction={activeTransaction}
                                IsDeposite={IsDepo}
                            />
                            :
                            <div className="w-full">
                                <button onClick={() => {
                                    setActiveTransaction(false)
                                    setIsDepo(true)
                                    setNewTran(!newtran)
                                }} className={`border rounded-md float-right border-gray-600 p-2 my-1 hover:bg-green-700 hover:stroke-white stroke-green px-3`}>
                                    <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                                 DEPOSITE
                         </button>
                                <button onClick={() => {
                                    setActiveTransaction(false)
                                    setIsDepo(false)
                                    setNewTran(!newtran)
                                }} className={`border rounded-md float-left border-gray-600 p-2 my-1 hover:bg-green-700 hover:stroke-white stroke-green px-3`}>
                                    <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                                 WITHDRAW
                         </button>
                            </div>
                    }

                    {
                        transaction && transaction.length > 0 ?
                            <table className="w-full bg-orange-100">
                                <thead className="bg-blue-700">
                                    <tr className="border p-2 text-white">
                                        <th className="border-r border-gray-400 p-2 text-center">#</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Amount</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Note</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Date</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Edit</th>
                                    </tr>
                                </thead>
                                <tbody className="">
                                    {
                                        transaction.map((tran, index) => (
                                            <tr key={index} className={`border cursor-pointer hover:bg-gray-200 text-center ${tran['deposit'] ? 'bg-green-100' : 'bg-red-100'} `}>
                                                <td className="border-r p-2 text-sm">{index + 1}</td>
                                                <td className="border-r text-gray-900 text-sm">{tran["deposit"] ? MoneyFormat(tran['deposit']) : MoneyFormat(tran["withdraw"])}</td>
                                                <td className="border-r text-gray-900 text-sm">{tran && tran.reason ? tran.reason : '--'}</td>
                                                <td className="border-r text-gray-900 text-sm">{moment(tran.created_at).format('ll')}</td>
                                                <td className="border-r text-gray-900 text-sm">
                                                    <div className="inline-flex">
                                                        <p onClick={() => {
                                                            setNewTran(false)
                                                            setActiveTransaction(tran)
                                                            setIsDepo(tran["deposit"] ? true : false)
                                                        }} className="text-gray-700 rounded p-1 mx-1 hover:text-white hover:bg-blue-400"> <Edit3 size={18} className={"inline"} /> </p>
                                                        <p onClick={() => {
                                                            setActiveTransaction(tran)
                                                            setShowPop(true)
                                                        }} className="text-gray-700 rounded p-1 mx-1 hover:text-white hover:bg-red-400"> <Trash2 size={18} className={"inline"} /> </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </table>
                            :
                            <div className="w-full text-center p-4">
                                <div className="">
                                    <p>
                                        <FolderPlus onClick={() => console.log('TT')} className="inline-flex text-center rounded-lg m-2 hover:bg-teal-100" size={140} color={'gray'} strokeWidth={0.5} />
                                    </p>
                                    <em>Transaction list is empty.</em>

                                </div>
                            </div>
                    }
                </Fragment>

            }

            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<PopupChild />}
                parentCss={'w-5/12 bg-white rounded p-6'}

            />
        </Fragment>


    )
}

export default TransactionList;