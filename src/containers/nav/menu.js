import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import { Clipboard, ChevronDown, ChevronRight, User, Home, Users, Monitor, Layers, List, Book, FileText, BookOpen , Lock } from 'react-feather';

import AuthService from '../../services/auth_service';
import { useAuth } from "../../providers/auth_provider";

import {useHistory} from '../../providers/history_provider';

// TODO : main links
// Clients
// ExRequest
// Resources
// Transactions
// Report
// Admin





const Menu = ({ path, toggleSideBar, setToggleSideBar }) => {


    const [active, setActive] = useState('Dashboard')
    const [activeSubMenu, setActiveSubMenu] = useState('')
    const { authState } = useAuth()
    const { roles } = authState
    const { history } = useHistory()
    

    const menuList = [
        {
            name: 'Dashboard',
            link: '/dashboard/',
            icon: <Home size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            role: ["personnel", "admin"]
        },
        {
            name: 'Client',
            icon: <Users size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            link: '/dashboard/client',
            role: ["personnel", "admin"]
        },
        {
            name: 'ExRequest',
            icon: <Layers size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            link: '/dashboard/exrequest',
            role: ["personnel", "admin"]
        },
        {
            name: 'Loan',
            icon: <Layers size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            link: '/dashboard/loan',
            role: ["personnel", "admin"]
        },
        {
            name: 'Memo',
            icon: <Book size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            link: '/dashboard/memo',
            role: ["personnel", "admin"]
        },
        // {
        //     name: 'Resources',
        //     icon: <List size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
        //     link: '/dashboard/resources',
        //     role: ["admin"]
        // },
        {
            name: 'Accounts',
            icon: <BookOpen size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            link: '/dashboard/account',
            role: ["admin"]
        },
        {
            name: 'Transactions',
            // link: '/dashboard/transaction',
            icon: <Monitor size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            submenu: [
                {
                    name: 'Debits',
                    link: '/dashboard/transaction/debit',
                    icon: '',
                    role: ["personnel", "admin"]
                },
                {
                    name: 'Credits',
                    link: '/dashboard/transaction/credit',
                    icon: '',
                    role: ["personnel", "admin"]
                },
            ]
        },
        {
            name: 'Report',
            link: '/dashboard/report',
            icon: <Clipboard size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            role: ["admin"]
        }, {
            name: 'Admin',
            link: '/dashboard/admin',
            icon: <User size={20} strokeWidth='2.5' className="inline-block text-gray-200" />,
            role: ["admin"]
        },
    ]


    // const Active = () => {
    //     return(
    // border-left-style: solid;
    // border-left-width: 3px;
    // content: " ";
    // height: 100%;
    // left: 0;
    // position: absolute;
    // top: 0;
    //     )
    // }



    function CheckPermission(rol) {
        const rol_perm = rol.map(r => roles.includes(r)).includes(true)
        return rol_perm
    }



    return (
        <ul className="relaive h-full overflow-y-auto pt-20">
            {
                menuList.map(mnu => {
                    return (

                        mnu.submenu ?

                            <li key={mnu.name} onClick={() => setActive(mnu.name)} className="text-gray-200 hover:text-white block p-2 m-2 relative">

                                {
                                    path == mnu.link ?
                                        <i className="h-full absolute left-0 top-0" style={{ borderLeftColor: '#FE8A7D', borderLeftStyle: "solid", borderLeftWidth: '3px' }}></i>
                                        :
                                        null
                                }

                                {/* <p className="hover:bg-gray-600"> */}

                                <span className="px-2 pr-4">
                                    {mnu.icon}
                                </span>

                                {mnu.name}
                                {
                                    active === mnu.name ?
                                        <ChevronDown size={18} strokeWidth='2' className="inline-block text-gray-200 float-right mr-2" />
                                        :
                                        <ChevronRight size={18} strokeWidth='2' className="inline-block text-gray-200 float-right mr-2" />
                                }
                                {/* </p> */}

                                {
                                    active === mnu.name && mnu.submenu && mnu.submenu.length > 0 ?
                                        <ul className="ml-4">
                                            {

                                                mnu.submenu.map(subm => {
                                                    return (
                                                        CheckPermission(subm.role) ?
                                                            <li key={subm.name} className="hover:bg-gray-600">
                                                                <Link to={subm.link} onClick={() => setActiveSubMenu(subm.name)} className={`text-sm hover:text-white block p-2 m-2 ${activeSubMenu === subm.name ? 'text-orange-400 font-semibold' : 'text-gray-200'} `}>
                                                                    <ChevronRight size={18} strokeWidth='2' className="inline-block text-gray-200 mr-1" />
                                                                    {subm.name}
                                                                </Link>
                                                            </li>
                                                            : null
                                                    )

                                                })
                                            }
                                        </ul>
                                        :
                                        null
                                }


                            </li>

                            :
                            CheckPermission(mnu.role) ?

                                <li key={mnu.name} className="hover:bg-gray-600">
                                    <Link to={mnu.link} onClick={() => setActive(mnu.name)} className=" text-gray-200 hover:text-white block p-2 m-2 relative">

                                        {
                                            path == mnu.link || path.startsWith(mnu.link) ?
                                                <i className="h-full absolute left-0 top-0" style={{ borderLeftColor: '#FE8A7D', borderLeftStyle: "solid", borderLeftWidth: '3px' }}></i>
                                                :
                                                null
                                        }

                                        <span className="px-2 pr-4">
                                            {mnu.icon}
                                        </span>

                                        { toggleSideBar ? "" : mnu.name}

                                    </Link>
                                </li>
                                : null

                    )

                })
            }


            <li className="bg-gray-800 hover:bg-gray-600">
                <Link to={'#'} onClick={() => history.push("/auth/logout") } className="text-gray-200 hover:text-white block p-2 m-2 relative">

                    <span className="px-2 pr-4">
                        <Lock size={20} strokeWidth='2.5' className="inline-block text-gray-200" />
                    </span>

                    <p className="inline">Lock</p>

                </Link>
            </li>

        </ul >
    )

}

export default Menu;