import React, { useState, useEffect } from 'react';
import CustomField from '../../components/form/custom_field';
import LModel from '../../services/api';
import CircularLoading from '../../components/loading/circular_loading';

export default ({ closeSave, reloadPage, editLoan }) => {

    const [newLoan, setNewLoan] = useState(editLoan && editLoan.id ? editLoan : { name: "", reason: "" })
    const [loading, setLoading] = useState(false)

    const saveRequest = () => {
        if (newLoan && newLoan.id ) {

            setLoading(true)
            LModel.update('loan', newLoan.id, newLoan)
                .then(res => {
                    setLoading(false)
                    closeSave()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    closeSave()
                })

        } else {

            setLoading(true)
            LModel.create('loan/', newLoan)
                .then(res => {
                    setLoading(false)
                    closeSave()
                    reloadPage()
                }
                ).catch(e => {
                    setLoading(false)
                    console.log(e)
                    closeSave()
                })

        }
    }


    const handleInputChange = (name, val) => {
        let tempLoan = { ...newLoan }
        tempLoan[name] = val
        setNewLoan(tempLoan)
    }



    return (
        <div className="rounded border border-2 border-blue-900 flex flex-col p-2 my-2 py-6 items-center">

            <CustomField
                name={`name`}
                placeholder={`${newLoan['name'] ? newLoan['name'] : 'name' }`}
                type="text"
                onBlur={() => console.log()}
                defaultValue={newLoan["name"]}
                onChange={e => handleInputChange('name', e)}
                divStyle="w-6/12 px-1 mb-6 md:mb-0"
                labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            />

            {/* <CustomField
                name={`taken`}
                placeholder={`${newLoan['taken'] ? newLoan['taken'] : 'loan amount' }`}
                type="number"
                onBlur={() => console.log()}
                defaultValue={newLoan["taken"]}
                onChange={e => handleInputChange('taken', e)}
                divStyle="w-6/12 px-1 mb-6 md:mb-0"
                labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                inputStyle="appearance-none bg-white block w-full text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            /> */}


            <CustomField
                name="reason"
                placeholder={`${newLoan['reason'] ? newLoan['reason'] : 'loan reason..' }`}
                type="textarea"
                onBlur={() => console.log()}
                label=""
                defaultValue={newLoan["reason"]}
                onChange={e => handleInputChange('reason', e)}
                divStyle="w-6/12 px-1 py-2 mb-6 md:mb-0"
                labelStyle="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-3"
                inputStyle="appearance-none block w-full bg-gray-200 text-gray-800 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            />


            <div className="w-full text-center my-2 px-8">
                <button onClick={() => saveRequest()} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{loading ? <CircularLoading loading={loading} />: newLoan.id ? "Update" : "Save"}</button>
                <button onClick={() => closeSave()} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
            </div>

        </div>
    )

}