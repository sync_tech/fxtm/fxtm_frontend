export const MoneyFormat = (num) => {
        return parseFloat(parseFloat(num ? num : 0).toFixed(2)).toLocaleString()
    }
