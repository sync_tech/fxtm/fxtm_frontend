import React, { useState, useEffect, Fragment } from 'react';
import { FolderPlus, Plus, Bookmark } from 'react-feather';
import moment from 'moment'
import AutoCompleteSearch from '../../components/exrequest/client_search';
import ExrequestLoading from '../../components/loading/exrequest_loading';
import ExRequestCard from '../../components/exrequest/exrequest_card';
import { useHistory } from '../../providers/history_provider';
import DeleteElement from '../../components/resource/delete_model';
import Pagination from '../../components/ui/pagination'
import Filter from '../../components/exrequest/filter';
import Popup from '../../components/ui/popup';
import LModel from '../../services/api';
import NewExrequest from './new_request'

const ListExRequest = (props) => {

    const [loading, setLoading] = useState(false)
    const [exRequest, setExRequest] = useState([])
    const [exRequestInfo, setExRequestInfo] = useState({ previous: null, next: null, count: 0 })
    const [toogleNew, setToogleNew] = useState(props && props.location && props.location.state && props.location.state.new ? true : false)
    const { history } = useHistory()

    const [filter, setFilter] = useState("&is_completed=false")
    const [reload, setReload] = useState(false)
    const [searchKey, setSearchKey] = useState(props && props.location && props.location.state && props.location.state.client ? props.location.state.client.name : "")
    const [autosetclient, setAutoSetClient] = useState(props && props.location && props.location.state && props.location.state.new && props.location.state.client ? props.location.state.client : null)
    const [circularloading, setCircularloading] = useState(false)
    const [paginationIndex, setPaginationIndex] = useState(1)

    const [showpop, setShowPop] = useState(false)
    const [activeExReq, setActiveExReq] = useState()

    const loadRequest = () => {

        setLoading(true)
        LModel.find('requests', null, filter !== null ? `${filter}&client__name=${searchKey}&page=${1}` : `client__name=${searchKey}&page=${paginationIndex}`)
            .then(res => {
                if (res && res.results) {
                    setLoading(false)
                    setExRequest(res.results)
                    setExRequestInfo(res)
                }
            }).catch(e => {
                setLoading(false)
                console.log(e)
            })

    }


    useEffect(() => {
        loadRequest()
    }, [searchKey, reload, filter, paginationIndex])

    useEffect(() => {
        if (activeExReq && activeExReq.id) {
            setToogleNew(true)
        }
        if (showpop) {
            setToogleNew(false)
        }
    }, [activeExReq])




    return (
        <div className="">
            <div className="flex justify-between">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadRequest}
                />

                <Filter
                    FilterMenu={[{
                        label: 'All',
                        value: null
                    },
                    {
                        label: 'VIP',
                        value: "&client__vip=true"
                    },
                    {
                        label: 'Not VIP',
                        value: "&client__vip=false"
                    },
                    {
                        label: 'Onproccess',
                        value: "&is_completed=false"
                    },
                    {
                        label: 'Complete',
                        value: "&is_completed=true"
                    },
                    {
                        label: 'Supplier',
                        value: "&is_supplier=true"
                    }

                    ]}
                    filter={filter}
                    setFilter={(e) => setFilter(e)}
                />
            </div>




            {
                loading ?
                    <div className="flex justify-center">
                        <ExrequestLoading />
                    </div>
                    :



                    <Fragment>

                        {!toogleNew ?
                            <div className="flex justify-end m-1">
                                <button onClick={() => {
                                    setActiveExReq()
                                    setToogleNew(true)
                                }} className="border rounded-md float-riht border-gray-600 p-2 my-1 hover:bg-green-700 hover:stroke-white stroke-green px-3">
                                    <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                                    ADD NEW
                                </button>
                            </div> :
                            <NewExrequest closeSave={() => setToogleNew(false)} reloadPage={() => setReload(!reload)} auto_set_client={autosetclient} activeExReq={activeExReq} />
                        }





                        {exRequest && !loading && exRequest.length > 0 ?
                            <div className="grid grid-cols-4 gap-10 mt-6 m-10">
                                {
                                    exRequest.map((exr, idx) => {
                                        return <ExRequestCard key={idx} req={exr}
                                            reload={() => setReload(!reload)}
                                            setShowPop={() => setShowPop(true)}
                                            closeNew={() => setToogleNew(false)}
                                            setActiveExReq={(acExReq) => setActiveExReq(acExReq)}
                                        />


                                    })
                                }
                            </div>

                            :
                            <div className="w-full text-center p-4">

                                <div className="">
                                    <p className={searchKey === undefined ? 'hidden' : 'block text-gray-800 text-2xl font-thin'}> {`Not Found ${searchKey}`}</p>
                                    <p>
                                        <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-white" size={140} color={'gray'} strokeWidth={0.5} />
                                    </p>
                                </div>

                            </div>
                        }

                    </Fragment>
            }

            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<DeleteElement
                    setShowPop={() => setShowPop(!showpop)}
                    activeElement={activeExReq}
                    msg={<Fragment><span className="text-blue-800 font-semibold italic mx-1"> {activeExReq && activeExReq.client && activeExReq.client.name ? `${activeExReq.client.name}'s` : '--'}</span> Exchange Request <span className="text-blue-800 font-semibold italic mx-1"> {moment(activeExReq && activeExReq.name ? activeExReq.name : null).isValid() ? moment(activeExReq.name).format("MMMM Do YY") : activeExReq && activeExReq.name ? activeExReq.name : ''} </span> and its Transactions </Fragment>}
                    url={"request"}
                    prev={null}
                    reload={() => setReload(true)} />}

                parentCss={'w-5/12 bg-white rounded p-6'}
            />


            <Pagination
                count={exRequestInfo.count}
                previous={exRequestInfo.previous}
                next={exRequestInfo.next}
                list={4}
                activeIndex={paginationIndex}
                setIndex={(i) => setPaginationIndex(i)}
            />

        </div>
    )

}

export default ListExRequest;