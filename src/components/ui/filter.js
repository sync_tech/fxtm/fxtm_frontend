import React, { useState, useEffect, useRef } from 'react'
import { ChevronDown, Filter, Clock, Calendar, CheckSquare, Plus } from 'react-feather'
import CustomField from '../form/custom_field';



export default ({ filter, setFilter }) => {
    const [show, setShow] = useState(false)
    const [iniD, setIniD] = useState(new Date())
    const [finalD, setFinalD] = useState(new Date())
    const [pickDate, setpickDate] = useState()

    const filterMenu = useRef(null);
    DropDownToggler(filterMenu);

    function DropDownToggler(ref) {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setShow(false)
            } else {
                setShow(true)
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside, true);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside, true);
            };
        });
    }



    const handleFilter = (e, d) => {
        let filt = {
            initialDate: new Date().toISOString(),
            finalDate: new Date().toISOString()
        }

        switch (d) {
            case "today":
                let td = new Date()
                let tdini = td.setHours(0, 0, 0, 0)
                let tdfinal = td.setDate(td.getDate() + 1)

                filt = {
                    initialDate: new Date(tdini).toISOString(),
                    finalDate: new Date(tdfinal).toISOString()
                }
                setFilter(filt)
                break;
            case "week":

                let wdate = new Date(new Date().setHours(0, 0, 0, 0))
                let firstDate = new Date(wdate.setDate(wdate.getDate() - wdate.getDay())).toISOString()
                let lastDate = new Date(wdate.setDate(wdate.getDate() - wdate.getDay() + 7)).toISOString()
                filt = {
                    initialDate: firstDate,
                    finalDate: lastDate
                }
                setFilter(filt)
                break;
            case "month":
                let mdate = new Date(new Date().setHours(0, 0, 0, 0))
                let mfirstDate = new Date(mdate.setDate(0)).toISOString()

                mdate = new Date(new Date().setHours(0, 0, 0, 0))
                let mlastDate = new Date(mdate.setMonth(mdate.getMonth() + 1))
                mlastDate = new Date(mlastDate.setDate(0)).toISOString()
                filt = {
                    initialDate: mfirstDate,
                    finalDate: mlastDate
                }
                setFilter(filt)
                break;
            case "pickdate":
                // setFilter(d)
                setpickDate("pickdate")
                break;
            case "all":
                filt = {
                    initialDate: '',
                    finalDate: ''
                }
                setFilter(filt)
                break;
            default:
                console.log("Default")
                filt = {
                    initialDate: new Date().toISOString(),
                    finalDate: new Date().toISOString()
                }
                setFilter(filt)
        }
    }

    useEffect(() => {
        setShow(false)
    }, [pickDate, filter])

    return (

        // {
        pickDate === "pickdate" ?
            <div className="flex flex-col">
                <div className="flex">
                    <CustomField
                        name="name"
                        type="date"
                        onBlur={() => console.log()}
                        value={new Date()}
                        onChange={e => setIniD(new Date(new Date(`${e}`).setHours(0, 0, 0, 0)))}
                        divStyle="mx-2"
                    />
                    <CustomField
                        name="name"
                        type="date"
                        value={new Date()}
                        onBlur={() => console.log()}
                        onChange={e => setFinalD(new Date(new Date(`${e}`).setHours(0, 0, 0, 0)))}
                    />
                </div>
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mx-2"
                    onClick={() => {
                        let filt = {
                            initialDate: null,
                            finalDate: null
                        }

                        if (iniD instanceof Date && !isNaN(iniD) && finalD instanceof Date && !isNaN(finalD)) {

                            setpickDate()
                            if (iniD > finalD) {
                                filt.initialDate = new Date(finalD).toISOString()
                                filt.finalDate = new Date(new Date(iniD).setDate(new Date(iniD).getDate() + 1)).toISOString()
                                setFilter(filt)
                            } else {

                                filt.initialDate = new Date(iniD).toISOString()
                                filt.finalDate = new Date(new Date(finalD).setDate(new Date(finalD).getDate() + 1)).toISOString()
                                setFilter(filt)
                            }
                        }

                    }}
                >Filter</button>
            </div>
            :
            <div ref={filterMenu} className="relative my-auto bg-white z-10">


                <button className="relative flex items-center justify-start lg:mb-0 mb-4 pointer-cursor p-2 px-4 rounded border border-gray-600">
                    <Filter size={15} />
                    <p className="text-gray-800 pl-2 pr-4">Filter </p>
                    <ChevronDown size={15} />
                </button>

                <ul className={`bg-white border-2 rounded absolute w-48 right-0 my-2 ${show ? "" : "hidden"}`}  >
                    <li className="m-2">
                        <div className="flex px-4 py-1 hover:bg-blue-200" onClick={(e, t) => handleFilter(e, "today")}>
                            <Clock className="text-gray-700 my-auto" size={20} />
                            <span className="px-4 text-gray-700">Today</span>
                        </div>
                    </li>

                    <li className="m-2">
                        <div className="flex px-4 py-1 hover:bg-blue-200" onClick={(e, t) => handleFilter(e, "week")}>
                            <Calendar className="text-gray-700 my-auto" size={20} />
                            <span className="absolute text-xxs text-gray-800 mt-2 ml-2">7</span>
                            <p className="mx-4 text-gray-700" >This Week</p>
                        </div>
                    </li>

                    <li className="m-2">
                        <div className="flex px-4 py-1 hover:bg-blue-200" onClick={(e, t) => handleFilter(e, "month")}>
                            <Calendar className="text-gray-700 my-auto" size={20} />
                            <span className="absolute text-xxs text-gray-800 mt-2 ml-1">30</span>
                            <p className="mx-4 text-gray-700" >This Month</p>
                        </div>
                    </li>

                    <li className="m-2">
                        <div className="flex px-4 py-1 hover:bg-blue-200" onClick={(e, t) => handleFilter(e, "pickdate")}>
                            <Calendar className="text-gray-700 my-auto" size={20} />
                            <p className="mx-4 text-gray-700" >Pick Date</p>
                        </div>
                    </li>

                    <li className="m-2">
                        <div className="flex px-4 py-1 hover:bg-blue-200" onClick={(e, t) => handleFilter(e, "all")}>
                            <CheckSquare className="text-gray-700 my-auto" size={20} />
                            <p className="mx-4 text-gray-700" >All</p>
                        </div>
                    </li>
                </ul>
            </div>


    )

}