import React from 'react'
import './d_loading.css'

export default ({ loading , txt , color}) => {
    return (
        <div className={`spinner ${!loading ? 'hidden': ''}`}>
            <span>{txt ? txt : " Loading "}  </span>
            <div className={`bounce1 ${color ? color : "bg-white" }`}></div>
            <div className={`bounce2 ${color ? color : "bg-white" }`}></div>
            <div className={`bounce3 ${color ? color : "bg-white" }`}></div>
        </div>

    )
}