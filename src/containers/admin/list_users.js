import React, { useState, useEffect, Fragment } from 'react';
import moment from 'moment';
import { FolderPlus, Plus } from 'react-feather';
import LModel from '../../services/api';
import Loading from '../../components/loading/dot_loading';
import NewUser from './new_user';
import { useHistory } from '../../providers/history_provider';
import CircularLoading from '../../components/loading/circular_loading';
import Popup from '../../components/ui/popup';

import UserGroupMgmnt from './user_group';


const ListUsers = () => {


    const [toogleNew, setToogleNew] = useState(false)
    const [userLoading, setUserLoading] = useState(false)
    const [user, setUser] = useState([])
    // const [totalDebit, setTotalDebit] = useState(0)
    const [deleteLoading, setDeleteLoading] = useState(false)
    const [reload, setReload] = useState(false)
    const [showpop, setShowPop] = useState(false)

    const [activeUser, setActiveUser] = useState({})
    const [popupChild, setPoppuChild] = useState()
    const { history } = useHistory()


    const loadUser = () => {

        setUserLoading(true)
        LModel.find('manage-user')
            .then(res => {
                setUserLoading(false)
                setUser(res)

            }
            ).catch(e => {
                setUserLoading(false)
                console.log(e)
            })

    }


    useEffect(() => {
        loadUser()
    }, [reload])


    // useEffect(() => {
    //     if (activeUser && activeUser.id) {
    //         setShowPop(true)
    //     }
    // }, [activeUser])


    // useEffect(() => {
    //     if (activeUser && activeUser) {
    //         setShowPop(true)
    //     }
    //     if (showpop) {

    //         setShowPop(false)

    //     }
    // }, [activeUser])



    const deleteMemo = (usr) => {
        
        if (usr && usr.id) {
            setDeleteLoading(true)
            LModel.destroy('manage-user', usr.id)
                .then(res => {
                    // if (res && res.results)  {
                    setDeleteLoading(false)
                    setShowPop(false)
                    loadUser()
                    // }
                }).catch(e => {
                    setDeleteLoading(false)
                    console.log(e)
                })
        }
    }



    const UserDeletePopUp = ({ usr }) => {


        return (
            <div className="w-full">
                <div className="text-left border-b-2 px-4">
                    <p className="text-lg font-semibold m-1 text-orange-600">Alert</p>
                </div>
                <div className="text-center p-3">
                    <p className="p-2 text-gray-700 text-xl">Are you sure, do you want delete
                    <span className="text-blue-800 font-semibold italic mx-1">{usr && usr.username ? usr.username : '--'}</span>
                    </p>
                </div>
                <div className="w-full text-right">
                    <button onClick={() => setShowPop(false)} className="border border-red-700 hover:bg-red-700 hover:text-white text-red-700 p-2 text-white font-bold rounded mr-4 w-32">Cancel</button>
                    <button onClick={() => deleteMemo(usr)} type="submit" className="disabled:bg-blue-200 bg-blue-700 p-2 text-white font-bold rounded mr-4 w-32">{deleteLoading ? <CircularLoading loading={deleteLoading} /> : "Yes"}</button>
                </div>
            </div>
        )
    }






    return (
        <div className="">

            {
                userLoading ?
                    <Loading loading={userLoading} color={'bg-gray-600'} />
                    :
                    user.length > 0 ?
                        <Fragment>
                            {!toogleNew ?
                                <button onClick={() => setToogleNew(true)} className="border rounded-md float-right border-gray-600 p-2 my-3 hover:bg-green-700 hover:stroke-white stroke-green px-3">
                                    <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                                    ADD NEW
                                </button> : null}
                            {
                                toogleNew ?
                                    <NewUser toogleNq={() => setToogleNew(!toogleNew)} reloadPage={() => setReload(!reload)} editUser={activeUser} /> : null
                            }

                            <table className="w-full bg-white">
                                <thead className="bg-blue-700">
                                    <tr className="border p-2 text-white">
                                        <th className="border-r border-gray-400 p-2 text-center">#</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Name</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">UserName</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Group</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">LastLogin</th>
                                        <th className="border-r border-gray-400 font-semibold text-center">Setting</th>
                                    </tr>
                                </thead>
                                <tbody className="">
                                    {
                                        user.map((usr, index) => (
                                            <tr key={index}
                                                onClick={() => {
                                                    setShowPop(true)
                                                    setPoppuChild(<UserGroupMgmnt setShowPop={()=>setShowPop(false)} usr={usr} />)
                                                }}
                                                className={`border cursor-pointer hover:bg-gray-200 text-center`}>
                                                <td className="border-r p-2 text-sm">{index + 1}</td>
                                                <td className="border-r text-gray-900 text-sm">{usr && usr.first_name ? usr.first_name + ' ' : '--'} {usr && usr.last_name ? ' ' + usr.last_name : '--'}</td>
                                                <td className="border-r text-gray-900 text-sm">{usr && usr.username ? usr.username : '--'}</td>
                                                <td className="border-r text-gray-900 text-sm">{usr && usr.usr_groups ? usr.usr_groups.map(g => <p key={g.name} className="capitalize p-1">{g.name}</p>) : '--'}</td>
                                                <td className="border-r text-gray-900 text-sm">{usr && usr.last_login ? moment(usr.last_login).format('ll') : ''}</td>
                                                <td className="border-r text-gray-900 text-sm">
                                                    <p onClick={(e) => {
                                                        e.stopPropagation()
                                                        setToogleNew(true)
                                                        setActiveUser(usr)
                                                    }} className="inline border rounded border-blue-600 mx-2 p-2">Edit</p>
                                                    <p onClick={(e) => {
                                                        e.stopPropagation()
                                                        setShowPop(true)
                                                        setPoppuChild(<UserDeletePopUp usr={usr} />)

                                                    }} className="inline border rounded border-blue-600 mx-2 p-2">Delete</p>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </table>
                        </Fragment>
                        :
                        <div className="w-full text-center p-4">
                            <div className="">
                                <p>
                                    <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-teal-100" size={140} color={'gray'} strokeWidth={0.5} />
                                </p>
                                <em>User list is empty.</em>
                            </div>
                        </div>
            }


            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={popupChild}
                parentCss={'w-5/12 bg-white rounded p-6'}
            />


        </div>

    )

}

export default ListUsers



// active_user
// edit 
// popup
// delete
// 