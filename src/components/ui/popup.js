import React from 'react'
import { X } from 'react-feather'



export default ({ show, close, children, parentCss }) => {

    if (show) {

        // useBodyClass(`overflow-y-hidden`)
        return (
            <div className="bg-gray-800 h-full right-0 top-0 w-full fixed overflow-y-scroll z-10" style={{ background: 'rgba(0,0,0,0.75)' }} >
                <X onClick={() => close(true)} className={'float-right absolute right-0 m-4'} size={30} color={'white'} strokeWidth={3} />
                
                <div className="flex items-center justify-center h-screen">

                    <div className={`${parentCss ? parentCss : 'rounded w-full rounded p-6'}`}>

                        {children}

                    </div>


                </div>
            </div>
        )
    } else {
        // useBodyClass()
        return null


    }

}