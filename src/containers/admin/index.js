import React from 'react';
import { Route } from 'react-router-dom';
import PrivateRoute from '../../components/route/private_route';
import { adminRoutes } from '../../routes';


export default () => {
    return (
        <div className="mx-4">
            <div className="p-4">

                {
                    adminRoutes.map((route, index) =>
                        route.private ? <PrivateRoute key={index} {...route} /> : <Route key={index} {...route} />
                    )
                }

            </div>
        </div>

    )
}