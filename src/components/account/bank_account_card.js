import React, { useState, useEffect, useRef } from 'react';
import { Edit, Trash2, MoreVertical } from 'react-feather'
import { MoneyFormat } from '../../services/money_format';


const AccountCard = ({ crncy, activeAccount, setActiveAccount, setShowPop, setToggleNew, setNewAccount }) => {
    const [show, setShow] = useState(false)

    const moreMenu = useRef(null);
    DropDownToggler(moreMenu);

    function DropDownToggler(ref) {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                setShow(false)
            } else {
                setShow(true)
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside, true);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside, true);
            };
        });
    }

    return (
        <div onClick={() => setActiveAccount(crncy)} className={`bg-white rounded-md hover:shadow-lg p-2 mb-3 break-all relative ${crncy === activeAccount ? 'bg-blue-200 border border-white' : 'border-4 border-white hover:border-blue-400'} `}>

            <div className="text-center px-2">
                <p className={`text-center font-thin text-lg ${crncy['balance'] && (crncy.balance['deposit'] >= crncy.balance['withdraw']) ? 'text-green-700' : 'text-red-700' }`}
                >{crncy['balance'] && (crncy.balance['deposit'] || crncy.balance['withdraw']) ? MoneyFormat(crncy.balance['deposit'] - crncy.balance['withdraw']) : 0 }</p>
                <div ref={moreMenu} className="absolute right-0 top-0">
                    {show ?
                        <div className="p-1">

                            <div
                                onClick={() => {
                                    setToggleNew(false)
                                    setNewAccount(crncy)
                                }}
                                className="text-gray-700 rounded hover:bg-gray-400 hover:text-blue-600">
                                <Edit size={16} className="inline m-1" />
                            </div>
                            <div
                                onClick={() => {
                                    setToggleNew(false)
                                    setShowPop()
                                }}
                                className="text-gray-700 rounded hover:bg-gray-400 hover:text-red-600">
                                <Trash2 size={16} className="inline m-1" />
                            </div>

                        </div>
                        :
                        <div onClick={() => {
                            setToggleNew(false)
                            setShow(!show)
                        }}
                            className=" text-gray-500 rounded hover:text-gray-800 m-1 p-1 hover:bg-gray-400">
                            <MoreVertical size={16} className="my-auto" />
                        </div>

                    }
                </div>
            </div>
            <p className="text-lg font-semibold text-center rounded  text-gray-700 hover:text-gray-800">{crncy.holder}</p>
            <p className="text-center rounded text-gray-700 hover:text-gray-800">{crncy.bid}</p>
        </div>

    )
}

export default AccountCard;