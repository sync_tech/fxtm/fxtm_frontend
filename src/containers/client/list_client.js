import React, { useState, useEffect, Fragment } from 'react';
import { FolderPlus, Plus } from 'react-feather'
import { Link } from 'react-router-dom';
import AutoCompleteSearch from '../../components/exrequest/client_search';
import ClientLoading from '../../components/loading/client_loading'
import ClientCard from '../../components/client/client_card';
import Pagination from '../../components/ui/pagination'
import Filter from '../../components/exrequest/filter';
import LModel from '../../services/api';
import NewClient from './new_client';
import Popup from '../../components/ui/popup';
import DeleteClient from './delete_client'

const ListClient = () => {

    const [clients, setClients] = useState([])
    const [clientInfo, setClientInfo] = useState({ previous: null, next: null, count: 0 })
    const [loading, setLoading] = useState(false)
    const [toogleNew, setToogleNew] = useState(false)
    const [reload, setReload] = useState(false)
    const [filter, setFilter] = useState("")

    const [searchKey, setSearchKey] = useState("")
    const [circularloading, setCircularloading] = useState(false)

    const [paginationIndex, setPaginationIndex] = useState(1)

    const [showpop, setShowPop] = useState(false)

    const [activeClient, setActiveClient] = useState()


    const loadClients = () => {
        setLoading(true)
        LModel.find('clients', null, filter !== null ? `${filter}&name=${searchKey}&page=${1}` : `name=${searchKey}&page=${paginationIndex}`)

            /*
            vip
            name
            pagination
            */

            .then(res => {
                setLoading(false)
                if (res && res.results) {
                    setClients(res.results)
                    setClientInfo(res)
                }
            }
            ).catch(e => {
                setLoading(false)
                console.log(e)
            })

    }



    useEffect(() => {
        loadClients()
    }, [searchKey, reload, filter, paginationIndex])




    useEffect(() => {
        if (activeClient && activeClient.id) {
            setToogleNew(true)
        }
        if (showpop) {
            setToogleNew(false)
        }
    }, [activeClient])


    return (
        <div className="">
            <div className="flex justify-between">
                <AutoCompleteSearch
                    searchKey={searchKey}
                    setSearchKey={(ky) => setSearchKey(ky)}
                    circularloading={circularloading}
                    loadSearchResult={loadClients}
                />


                <Filter
                    FilterMenu={[
                        {
                            label: 'All',
                            value: null
                        },
                        {
                            label: 'VIP',
                            value: 'vip=true'
                        },
                        {
                            label: 'Not VIP',
                            value: 'vip=false'
                        }
                    ]}
                    filter={filter}
                    setFilter={(e) => setFilter(e)}
                />



            </div>




            {
                loading ?
                    <div className="flex justify-center">
                        <ClientLoading />
                    </div>
                    :

                    <Fragment>
                        {

                            !toogleNew ?
                                <div className="flex justify-end m-1">
                                    <button onClick={() => {
                                        setActiveClient()
                                        setToogleNew(true)
                                    }} className="border rounded-md float-riht border-gray-600 p-2 my-1 hover:bg-green-700 hover:stroke-white stroke-green px-3">
                                        <Plus size={22} strokeWidth="4" className="mr-1 inline" />
                                        ADD NEW
                                    </button>
                                </div>
                                :
                                <NewClient toogleC={() => setToogleNew(false)} reload={() => setReload(!reload)} activeClient={activeClient} />
                        }


                        {clients && !loading && clients.length > 0 ?
                            <div className="grid sm:grid-cols-1 md:grid-cols-3 gap-5">

                                {
                                    clients.map((cnt, i) => {
                                        return (
                                            <ClientCard key={i}
                                                data={cnt}
                                                setShowPop={() => setShowPop(true)}
                                                closeNew={() => setToogleNew(false)}
                                                setActiveClient={(clnt) => setActiveClient(clnt)} />
                                        )
                                    })
                                }

                            </div>
                            :
                            <div className="w-full text-center p-4">

                                <div className="">
                                    <p className={searchKey === undefined ? 'hidden' : 'block text-gray-800 text-2xl font-thin'}> {`Not Found ${searchKey}`}</p>
                                    <p>
                                        <FolderPlus onClick={() => setToogleNew(true)} className="inline-flex text-center rounded-lg m-2 hover:bg-white" size={140} color={'gray'} strokeWidth={0.5} />
                                    </p>
                                </div>

                            </div>



                        }

                    </Fragment>


            }

            <Popup
                show={showpop}
                close={() => setShowPop(!showpop)}
                children={<DeleteClient setShowPop={() => setShowPop(!showpop)} activeClient={activeClient} reload={() => setReload(true)} />}
                parentCss={'w-5/12 bg-white rounded p-6'}
            />

            <Pagination
                count={clientInfo.count}
                previous={clientInfo.previous}
                next={clientInfo.next}
                list={12}
                activeIndex={paginationIndex}
                setIndex={(i) => setPaginationIndex(i)}
            />
        </div >

    )
}

export default ListClient;